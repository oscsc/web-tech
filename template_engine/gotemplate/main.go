package main

import (
	"fmt"
	"github.com/Masterminds/sprig/v3"
	"gopkg.in/yaml.v3"
	"log"
	"os"
	"text/template"
)

func main() {
	// read the templates, set the 3rd party funcs such as indent
	files, err := template.New("template").Funcs(template.FuncMap(sprig.FuncMap())).ParseFiles("template.yaml")
	if err != nil {
		log.Printf("解析错误")
		os.Exit(-1)
	}
	templates := files.Templates()[0]

	// read the values
	valueFile := "values.yaml"
	f, err := os.Open(valueFile)
	if err != nil {
		fmt.Printf("open file %s err = %v \n", valueFile, err)
		return
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			panic(err)
		}
	}(f)

	decode := yaml.NewDecoder(f)
	argsMap := make(map[string]interface{})
	argsMap["host"] = "0 A\n1 B\n2 C"
	err = decode.Decode(&argsMap)
	if err != nil {
		fmt.Printf("yaml decode has error:%v\n", err)
		return
	}

	// create the output yaml and replace
	open, err := os.Create("t_out.yaml")
	defer func(open *os.File) {
		err := open.Close()
		if err != nil {
			panic(err)
		}
	}(open)

	err = templates.Execute(open, argsMap)
	if err != nil {
		log.Println("模板替换异常", err)
		os.Exit(-1)
	}

}
