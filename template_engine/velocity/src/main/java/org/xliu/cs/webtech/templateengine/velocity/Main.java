package org.xliu.cs.webtech.templateengine.velocity;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.IOException;
import java.io.StringWriter;

public class Main {
    public static void main(String[] args) throws IOException {
        //得到VelocityEngine
        VelocityEngine ve = new VelocityEngine();
        // classloader 搜索文件路径
        ve.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        ve.init();

        Template template = ve.getTemplate("jupyter-pod.yml", "UTF-8");

        VelocityContext data = new VelocityContext();

        data.put("NAME", "jupyter-task-id-1111");
        data.put("NAMESPACE", "ai-test");
        data.put("COMMAND",   "/bin/bash");
        data.put("CONTAINER_NAME",   "jupyter");
        data.put("IMAGE_NAME",   "jupyter-image:2.2");
        data.put("U_ID", "u_10001");

        StringWriter writer = new StringWriter();
        template.merge(data, writer);
        // or can use ve.evaluate method
        writer.close();
        System.out.println(writer);
    }
}
