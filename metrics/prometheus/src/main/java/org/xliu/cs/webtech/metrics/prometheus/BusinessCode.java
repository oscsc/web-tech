package org.xliu.cs.webtech.metrics.prometheus;

import io.prometheus.client.Summary;
import io.prometheus.client.exporter.HTTPServer;

import java.io.IOException;

/**
 * HttpServer with prometheus metrics
 */
public class BusinessCode {
    static final Summary requestLatency = Summary.build()
            .namespace("userservice").subsystem("event").name("handletime").unit("ms")
            .help("Time for handling event.")
            .quantile(0.5, 0.01)
            .quantile(0.9, 0.01)
            .quantile(0.99, 0.01)
            .labelNames("type", "name").register();


    public void run(String type, String name) throws InterruptedException {

        long startTime = System.currentTimeMillis();
        try {
            // Your code here.
            Thread.sleep((long) (Math.random() * 2000 + 500));
        } finally {
            long stopTime = System.currentTimeMillis();
            requestLatency.labels(type, name).observe(stopTime - startTime);
        }

    }


    public static void main(String[] args) throws InterruptedException, IOException {
        HTTPServer server = new HTTPServer.Builder()
                .withPort(1234)
                .build();

        BusinessCode code = new BusinessCode();
        String typeEnumClass;
        String typeEnumName;
        for (; ; ) {
            typeEnumClass = "Class" + (int) (Math.random() * 2);
            typeEnumName = "Name" + (int) (Math.random() * 2);
            code.run(typeEnumClass, typeEnumName);
        }
    }
}
