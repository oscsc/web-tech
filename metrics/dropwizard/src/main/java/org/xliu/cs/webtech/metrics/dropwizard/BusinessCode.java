package org.xliu.cs.webtech.metrics.dropwizard;

import com.codahale.metrics.*;
import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.health.annotation.Async;

import java.util.concurrent.TimeUnit;

public class BusinessCode {

    @Async(period = 2)
    private static class ServerHealthCheck extends HealthCheck {
        @Override
        public HealthCheck.Result check() throws Exception {
            double value = Math.random();

            if (value > 0.5) {
                System.err.println("server is healthy");
                return HealthCheck.Result.healthy();
            } else {
                System.err.println("server is unhealthy");
                return HealthCheck.Result.unhealthy("error random");
            }
        }
    }

    // thread-safe
    private final MetricRegistry metricRegistry = new MetricRegistry();
    final HealthCheckRegistry healthChecks = new HealthCheckRegistry();

    private final Meter requests;
    private final Histogram histogram;
    private final Counter counter;

    private int idx = 0;

    public BusinessCode() {
        // 默认是为 "metrics" 的 Log
        ScheduledReporter reporter = Slf4jReporter.forRegistry(metricRegistry)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();

        requests = metricRegistry.meter("requests");
        histogram = metricRegistry.histogram("histogram");

        // 每次计算时都会重新调用函数获得最新的 idx 的值
        metricRegistry.register(MetricRegistry.name("call", "times"), (Gauge<Integer>) () -> idx);

        //
        counter = metricRegistry.counter("count");

        healthChecks.register("server", new ServerHealthCheck());
        // 每 5 秒通过日志输出
        reporter.start(1, TimeUnit.SECONDS);

    }

    void run() {
        requests.mark();
        histogram.update((int)(Math.random() * (3)));
        idx++;

        int sign = Math.random() * 2 - 1 > 0 ? 1 : -1;
        counter.inc(((int)(Math.random() * 10)) * sign);
    }

    public static void main(String[] args) throws InterruptedException {
        BusinessCode businessCode = new BusinessCode();

        for (int i = 0; i < 10; i++) {
            businessCode.run();
            Thread.sleep(3000);
        }
    }
}
