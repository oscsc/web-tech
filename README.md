# WebTech

[认证与授权]
- [casbin]()
- [注解实现数据权限](./authz/spring-data-permission/README.md)

[接口管理方案介绍](https://xliuqq.github.io/blog_md/web/backend/api_manager.html)
- [根据OpenAPI yaml生成Server代码桩](api-doc/springboot-openapi-generator-code/README.md)
- [根据 OpenAPI 注解规范代码生成 Yaml 接口文档](api-doc/springboot-springdoc-openapi-generate-yaml/README.md)


[**操作日志方案介绍**](https://xliuqq.github.io/blog_md/web/backend/operation_log.html)
- [功能性操作日志](operation_log/bussiness-log)
- [业务型操作日志](operation_log/function-log)


[**消息实时推送方案介绍**](https://xliuqq.github.io/blog_md/web/msg_notify.html)
- [LongPulling](message_notify/LongPulling/README.md)
- [ServerSentEvent](message_notify/SSE/README.md)
- [WebSocket](message_notify/websocket/README.md)


[模板引擎](https://xliuqq.github.io/blog_md/web/template_engine.html)
- [Apache Velocity使用](template_engine/velocity/README.md)
- [Go Template](template_engine/gotemplate/README.md)


[视频播放方案介绍](https://xliuqq.github.io/blog_md/web/video_play.html))
- [视频播放](video_play/README.md)


[SpringCloud介绍](https://xliuqq.github.io/blog_md/web/backend/springcloud.html)
- [Demo应用](microservice/springcloud/README.md)

[Metrics介绍](https://xliuqq.github.io/blog_md/web/backend/metrics.html)
- [DropWizard Metrics](metrics/dropwizard)
- [Prometheus Metrics](metrics/prometheus)

切面:
- [Hutools AOP-JDK](aop/hutools-jdk)
- [Hutools AOP-CGLIB](aop/hutools-cglib)
- [Google Guice AOP]()

有限状态机（FSM）：
- [Spring StateMachine](./fsm/spring_statemachine)
- [Stateless4j](./fsm/stateless4j)

工具
- [(TODO)MinIO和LocalFS统一API](./tools/fileop)

图像
- [图像处理](./image/README.md)
  - 图像压缩、图像元数据更新
  - 人脸识别