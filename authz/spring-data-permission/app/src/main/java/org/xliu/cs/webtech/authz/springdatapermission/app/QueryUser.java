package org.xliu.cs.webtech.authz.springdatapermission.app;

import lombok.Data;

import java.util.List;

@Data
public class QueryUser {
    private List<String> ids;
}
