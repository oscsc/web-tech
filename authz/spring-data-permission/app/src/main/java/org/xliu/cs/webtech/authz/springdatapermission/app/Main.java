package org.xliu.cs.webtech.authz.springdatapermission.app;

import org.xliu.cs.webtech.authz.springdatapermission.annotation.EnableDataPermission;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDataPermission
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
