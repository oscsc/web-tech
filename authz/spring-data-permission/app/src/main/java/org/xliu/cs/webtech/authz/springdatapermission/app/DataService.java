package org.xliu.cs.webtech.authz.springdatapermission.app;


import org.springframework.stereotype.Service;
import org.xliu.cs.webtech.authz.springdatapermission.ScopeAuthAdapter;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DataService {

    private final Map<String, String> users = new HashMap<>();

    public DataService() {
        users.put("alice", "I am alice.");
        users.put("bob", "I am bob.");
        users.put("teddy", "I am teddy.");
    }

    public List<String> queryUsers() {
        return new ArrayList<>(users.values());
    }

    public String queryUser(String userId) {
        return users.getOrDefault(userId, "User not exist.");
    }

    /**
     * 查询多个用户，如果 userId 为 null，则查询所有用户。
     */
    public List<String> queryUsers(List<String> userIds) {
        // Note: 资源ids可能会包含 *
        if (userIds == null || userIds.contains(ScopeAuthAdapter.ALL_SCOPE)) {
            return queryUsers();
        }

        return userIds.stream().map(this::queryUser).collect(Collectors.toList());
    }
}
