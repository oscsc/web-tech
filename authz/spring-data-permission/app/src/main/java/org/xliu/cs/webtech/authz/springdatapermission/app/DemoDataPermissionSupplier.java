package org.xliu.cs.webtech.authz.springdatapermission.app;

import org.xliu.cs.webtech.authz.springdatapermission.DataPermissionSupplier;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DemoDataPermissionSupplier implements DataPermissionSupplier {

    @Override
    public Set<String> queryResourceIds(String key) {

        return Arrays.stream(key.split(",")).collect(Collectors.toSet());
    }
}
