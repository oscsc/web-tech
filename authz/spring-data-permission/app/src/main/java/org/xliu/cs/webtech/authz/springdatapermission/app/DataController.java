package org.xliu.cs.webtech.authz.springdatapermission.app;

import org.xliu.cs.webtech.authz.springdatapermission.annotation.ScopeAuth;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DataController {

    private final DataService dataService;

    public DataController(DataService dataService) {
        this.dataService = dataService;
    }

    /**
     * 查询单个用户，判断数据权限。
     */
    @ScopeAuth(rid = "#userId")
    @GetMapping("/user")
    public String query(@RequestParam String userId) {
        return dataService.queryUser(userId);
    }

    /**
     * 查询多个用户（如果无参，则表示查询所有用户），判断数据权限，
     *
     * Note: 只有类定义，可以通过 SPEL 进行修改
     */
    @ScopeAuth(rids = {"#user.ids"})
    @PostMapping("/users")
    public List<String> query(@RequestBody QueryUser user) {
        return dataService.queryUsers(user.getIds());
    }

}
