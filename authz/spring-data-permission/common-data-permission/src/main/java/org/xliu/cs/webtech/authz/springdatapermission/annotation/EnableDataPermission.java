package org.xliu.cs.webtech.authz.springdatapermission.annotation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Documented
@Import(ScopeAuthAdvice.class)
public @interface EnableDataPermission {
}