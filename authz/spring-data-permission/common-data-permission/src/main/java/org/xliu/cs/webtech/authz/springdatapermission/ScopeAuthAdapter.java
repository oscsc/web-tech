package org.xliu.cs.webtech.authz.springdatapermission;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
public class ScopeAuthAdapter {

    private final DataPermissionSupplier supplier;

    public static final String ALL_SCOPE = "*";

    public ScopeAuthAdapter(DataPermissionSupplier supplier) {
        this.supplier = supplier;
    }

    /**
     * 验证权限范围
     */
    public boolean identifyPermissionScope(String token, String resourceId) {
        Set<String> authorizeScope = supplier.queryResourceIds(token);

        if (authorizeScope == null) {
            return false;
        }

        if (authorizeScope.contains(ALL_SCOPE)) {
            // 如果是全开放则返回请求范围
            return true;
        }

        return authorizeScope.contains(resourceId);
    }

    /**
     * 验证权限范围
     */
    public Set<String> getOwnerResourceIds(String token, Set<String> resourceIds) {

        Set<String> authorizeScope = supplier.queryResourceIds(token);

        if (authorizeScope == null) {
            return Collections.emptySet();
        }

        // （List场景）没有请求资源，表明查看所有资源
        if (resourceIds == null || resourceIds.isEmpty()) {
            // 如果是全开放则返回请求范围，针对 “*“ 的情况，Service 端需要额外处理
            return authorizeScope;
        }
        // (多Get场景）
        if (authorizeScope.contains(ALL_SCOPE)) {
            // Note: 请求的资源，不会包含”*“ 这种情况
            resourceIds.remove(ALL_SCOPE);
            return resourceIds;
        }

        // 不修改入参 resourceIds
        Set<String> result = new HashSet<>(resourceIds);
        result.retainAll(authorizeScope);

        return result;
    }
}
