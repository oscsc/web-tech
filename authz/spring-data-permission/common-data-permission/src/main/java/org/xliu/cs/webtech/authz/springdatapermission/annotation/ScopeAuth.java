package org.xliu.cs.webtech.authz.springdatapermission.annotation;

import java.lang.annotation.*;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD})
@Documented
public @interface ScopeAuth {

    String token() default "AUTH_TOKEN";

    /**
     * resource id. rid, rids 只能存在一个
     * 支持 SPEL 表达式，如 '#id'
     */
    String rid() default "";


    /**
     * resource ids. rid, rids只能存在一个
     * 支持 SPEL 表达式（仅支持有且一个为表达式），如 '#ids'，其解析结果应该是 List < String >
     */
    String[] rids() default {};

}