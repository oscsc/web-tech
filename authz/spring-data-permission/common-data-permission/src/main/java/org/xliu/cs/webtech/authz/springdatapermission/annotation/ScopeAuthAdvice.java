package org.xliu.cs.webtech.authz.springdatapermission.annotation;

import org.xliu.cs.webtech.authz.springdatapermission.DataPermissionSupplier;
import org.xliu.cs.webtech.authz.springdatapermission.ScopeAuthAdapter;
import org.xliu.cs.webtech.authz.springdatapermission.util.AccessDeniedException;
import org.xliu.cs.webtech.authz.springdatapermission.util.ParseSPEL;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * 数据权限的切面处理。
 */
@Aspect
@Component
public class ScopeAuthAdvice {
    @Autowired
    @Lazy
    private DataPermissionSupplier supplier;
    private final String SPEL_FLAG = "#";
    @Around("@annotation(scopeAuth)")
    public Object before(ProceedingJoinPoint thisJoinPoint, ScopeAuth scopeAuth) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) thisJoinPoint.getSignature();
        // 获取方法参数
        Object[] args = thisJoinPoint.getArgs();

        // 获取token
        String authToken = getToken(scopeAuth.token());
        if (!StringUtils.hasLength(authToken)) {
            throw new AccessDeniedException("110", "没有找到AUTH_TOKEN");
        }

        // 单个资源id的权限
        if (StringUtils.hasLength(scopeAuth.rid())) {
            check(scopeAuth.rid(), methodSignature, args, authToken);
        }

        // 多个资源id的权限
        if (scopeAuth.rids().length != 0) {
            checks(scopeAuth.rids(), methodSignature, args, authToken);
        }

        return thisJoinPoint.proceed();
    }

    /**
     * 检查是否具备单个资源id的权限
     */
    private void check(String ridOrSpel, MethodSignature methodSignature, Object[] args, String authToken) {
        // 获取请求的数据范围
        String requestResourceId = getResourceId(args, ridOrSpel, methodSignature.getMethod());

        // 没有请求的资源ids
        if (!StringUtils.hasLength(requestResourceId)) {
            throw new AccessDeniedException("102", "未指定要访问的资源");
        }

        // 验证是否具备请求的数据范围
        ScopeAuthAdapter adapter = new ScopeAuthAdapter(supplier);
        if (!adapter.identifyPermissionScope(authToken, requestResourceId)) {
            throw new AccessDeniedException("101", "不具备访问该资源的权限");
        }
    }

    /**
     * 检查是否具备单个资源id的权限
     */
    private void checks(String[] ridOrSpels, MethodSignature methodSignature, Object[] args, String authToken) {
        Method method = methodSignature.getMethod();
        for (String ridOrSpel : ridOrSpels) {
            Set<String> rids = new HashSet<>();
            if (!StringUtils.hasLength(ridOrSpel)) {
                continue;
            }
            boolean hasSpel = ridOrSpel.indexOf(SPEL_FLAG) == 0;
            // 获取请求的数据范围
            if (hasSpel) {
                // 解析 SPEL 表达式，返回类型应该是 Collection<String>
                Collection<String> cols = ParseSPEL.parseMethodKey(ridOrSpel, method, args, Collection.class);
                if (cols != null) {
                    rids.addAll(cols);
                }
            } else {
                // 如果不是表达式，则返回值
                rids.add(ridOrSpel);
            }

            // 验证是否具备请求的数据范围
            ScopeAuthAdapter adapter = new ScopeAuthAdapter(supplier);
            Set<String> ownRids = adapter.getOwnerResourceIds(authToken, rids);
            if (ownRids.isEmpty()) {
                throw new AccessDeniedException("101", "不具备访问该资源的权限");
            }

            // 如果存在 SPEL 表达式，则需要重新设置资源ids
            if (hasSpel) {
                ParseSPEL.setMethodValue(ridOrSpel, ownRids, methodSignature.getMethod(), args);
            }
        }

    }

    /**
     * 获取 token
     */
    private String getToken(String tokenKey) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        tokenKey = StringUtils.hasLength(tokenKey) ? tokenKey: "AUTH_TOKEN";

        String token = request.getHeader(tokenKey);
        if (!StringUtils.hasLength(token)) {
            throw new AccessDeniedException("103", "请求不具备" + tokenKey);
        }

        return token;
    }


    /**
     * 获取请求权限范围
     */
    private String getResourceId(Object[] args, String ridOrSpel, Method method) {
        if (!StringUtils.hasLength(ridOrSpel)) {
            return null;
        }

        // 解析 SPEL 表达式
        if (ridOrSpel.indexOf(SPEL_FLAG) == 0) {
            return ParseSPEL.parseMethodKey(ridOrSpel, method, args, String.class);
        }

        // 如果不是表达式，则返回值
        return ridOrSpel;
    }
}