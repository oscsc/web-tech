package org.xliu.cs.webtech.authz.springdatapermission;

import java.util.Set;

public interface DataPermissionSupplier {

    /**
     * 查询范围
     * TODO: 获取用户某种类型资源的数据范围
     */
    Set<String> queryResourceIds(String key);
}