package org.xliu.cs.webtech.authz.springdatapermission.rest;

import org.casbin.jcasbin.main.Enforcer;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AuthzController {

    private final Enforcer enforcer;

    public AuthzController(Enforcer enforcer) {
        this.enforcer = enforcer;
    }

    /**
     * 角色添加 Permissions
     *    ROLE_USER, /services/data/users/*, *
     *    ROLE_ADMIN, /services/data/users/*, *
     *    ROLE_ADMIN, /services/data/admins/*, *
     */
    @PostMapping("/permissions")
    void addPolicy(@RequestBody List<List<String>> p) {
        // user may be user or role
        // addPermissionForUser（user, ...） => addPolicy(user, ...) => addNamedPolicies("p", user, ...)
        enforcer.addPolicies(p);
    }

    /**
     * 用户添加角色
     *    g, alice, ROLE_USER
     *    g, bob, ROLE_USER
     *    g, bob, ROLE_ADMIN
     */
    @PostMapping("/roles")
    void addRoles(@RequestBody List<List<String>> g) {
        // addRoleForUser（用户添加角色） => addNamedPolicy("g", ...)
        enforcer.addGroupingPolicies(g);
    }

    @GetMapping("/policy")
    List<List<String>> getAllPermissions(@RequestParam String user) {
        // 包含角色继承来的权限
        return enforcer.getImplicitPermissionsForUser(user);
    }

    /**
     * 删除某个角色的的的权限
     */
    @DeleteMapping("/policy")
    boolean deletePermissionsForRole(String role, String path, String method) {
        return enforcer.deletePermissionForUser(role, path, method);
    }

    @GetMapping("/direct_policy")
    List<List<String>> getDirectPermissions(@RequestParam String user) {
        // 不包含角色继承来的权限
        return enforcer.getPermissionsForUser(user);
    }

    /**
     * 接口权限校验
     */
    @GetMapping("/enforce")
    boolean hasPermission(@RequestParam String user, @RequestParam String path, @RequestParam String method) {
        return enforcer.enforce(user, path, method);
    }
}
