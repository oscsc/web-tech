package org.xliu.cs.webtech.videoplay;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

@Controller
@RequestMapping(("/m3u8"))
public class M3U8Controller {

    @GetMapping("/key/{name}")
    public void key(@PathVariable String name, HttpServletResponse response) throws IOException {
        try (FileInputStream in = new FileInputStream("./video_play/data/m3u8/" + name);
             OutputStream outputStream = response.getOutputStream()) {
            IOUtils.copy(in, outputStream);
        }
    }

    @GetMapping("/ts/{name}")
    public void ts(@PathVariable String name, HttpServletResponse response) {
        System.out.println("get ts: " + name);
        try (FileInputStream in = new FileInputStream("./video_play/data/m3u8/" +name);
             OutputStream outputStream = response.getOutputStream()) {
            System.out.println("get ts: " + name + " begin");
            IOUtils.copy(in, outputStream);
            System.out.println("get ts: " + name + " end");
        } catch (Exception e) {
            System.out.println("exception: " + e);
        }
        System.out.println("get ts: " + name + " over");
    }
}
