package org.xliu.cs.webtech.videoplay;

import org.bytedeco.ffmpeg.global.avcodec;
import org.bytedeco.ffmpeg.global.avutil;
import org.bytedeco.javacv.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FFmpegProcessor {

    public static void convertMediaToM3u8(InputStream inputStream, String webPrefix, String m3u8Url, String infoUrl) throws IOException {
        avutil.av_log_set_level(avutil.AV_LOG_INFO);

        FFmpegLogCallback.set();

        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(inputStream);
        grabber.start();

        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(m3u8Url, grabber.getImageWidth(), grabber.getImageHeight(), grabber.getAudioChannels());

        recorder.setFormat("hls");
        // 浮点数，设置每一片时长
        recorder.setOption("hls_time", "60");
        // 整数，设置M3U8中分片的个数
        recorder.setOption("hls_list_size", "0");
        // 标签，设置M3U8文件列表的操作，具体如下。
        // singlefile：生成一个媒体文件索引与字节范围
        // delete_segments：删除M3U8文件中不包含的过期的TS切片文件
        // rounddurations：生成的M3U8切片信息的 duration 为整数
        // discont_start：生成M3U8的时候在列表前边加上 discontinuity 标签
        // omit_endlist：在M3U8末尾不追加endlist标签
        recorder.setOption("hls_flags", "delete_segments");
        recorder.setOption("hls_delete_threshold", "1");
        recorder.setOption("hls_segment_type", "mpegts");
        // 设置切片名模板
        recorder.setOption("hls_segment_filename", webPrefix + "test-%d.ts");
        // 设置M3U8加密的key 文件路径
        recorder.setOption("hls_key_info_file", infoUrl);
        // 设置HTTP属性
        recorder.setOption("method", "POST");

        recorder.setFrameRate(25);
        recorder.setGopSize(2 * 25);
        recorder.setVideoQuality(1.0);
        recorder.setVideoBitrate(10 * 1024);
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
        recorder.setAudioCodec(avcodec.AV_CODEC_ID_AAC);
        recorder.start();

        Frame frame;
        while ((frame = grabber.grabImage()) != null) {
            try {
                recorder.record(frame);
            } catch (FrameRecorder.Exception e) {
                e.printStackTrace();
            }
        }
        recorder.setTimestamp(grabber.getTimestamp());
        recorder.close();
        grabber.close();
    }

    public static void main(String[] args) throws IOException {
        // if running is idea, the user.dir is "web-tech" dir not "video_play" dir
        System.out.println(System.getProperty("user.dir"));

        FileInputStream inputStream = new FileInputStream("./video_play/data/test.mp4");
        // webPrefix 可以是本地路劲，或者 http 接口（提供读写流即可）
        String webPrefix = "./video_play/data/m3u8/";
        String m3u8Url = webPrefix + "test.m3u8";
        String infoUrl = webPrefix + "test.info";

        FFmpegProcessor.convertMediaToM3u8(inputStream, webPrefix, m3u8Url, infoUrl);
    }
}
