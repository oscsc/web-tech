import zhLocale from "element-ui/lib/locale/lang/zh-CN";
const cn = {
    message: {
        name: "姓名",
        salaryInMonth: "月薪",
        salaryInYear: "年薪",
        selectLanguage: "选择语言"
    },
    ...zhLocale
};

export default cn;