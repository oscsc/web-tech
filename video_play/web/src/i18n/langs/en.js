import enLocale from "element-ui/lib/locale/lang/en";
const en = {
    message: {
        name: "name",
        salaryInMonth: "salaryInMonth",
        salaryInYear: "salaryInYear",
        selectLanguage: "Select Language"
    },
    ...enLocale
};

export default en;