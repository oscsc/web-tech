package org.xliu.cs.webtech.aop.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.matcher.Matchers;

// 定义 Injector
class GuiceInjector {
    public final static Injector injector = Guice.createInjector(new MainModule());

    private static class MainModule extends AbstractModule {
        @Override
        protected void configure() {
            // 默认不是单例模式，需要指定 in scope 或者在实现类上使用 @Singleton
            bind(Animal.class).to(Cat.class).in(Scopes.SINGLETON);
            // 绑定一个实例，永远是单例
            // bind(Animal.class).toInstance(new Cat());
            // 泛型绑定
            // bind(new TypeLiteral<List<Animal>>(){}).toInstance(Arrays.asList(new Dog(),new Cat()));

            // AOP, 对匹配的类/匹配的方法，执行切面函数
            bindInterceptor(Matchers.subclassesOf(Animal.class), Matchers.any(), new AopFunc());

            // AOP, 对任意类的匹配注解的方法，执行切面函数
            // bindInterceptor(Matchers.any(), Matchers.annotatedWith(Target.class), new AopFunc());
        }
    }

}