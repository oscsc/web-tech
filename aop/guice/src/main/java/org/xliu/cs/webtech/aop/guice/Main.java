package org.xliu.cs.webtech.aop.guice;

import com.google.inject.Inject;

public class Main {

    @Inject
    private Animal animal;

    public static void main(String[] args) {
        Main main = new Main();
        // 或者 injector.getInstance(Animal.class) 可以获取对应的实现类
        GuiceInjector.injector.injectMembers(main);

        // cat
        main.animal.eat();
    }
}
