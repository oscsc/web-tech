package org.xliu.cs.webtech.aop.guice;


import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

// 定义方法拦截器
public class AopFunc implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        String name = invocation.getMethod().getName();
        long startTime = System.nanoTime();
        System.out.printf("before method[%s] at %s%n", name, startTime);

        Object obj = null;
        try {
            obj = invocation.proceed();// 执行服务
        } finally {
            long endTime = System.nanoTime();
            System.out.printf("after method[%s] at %s, cost(ns):%d%n", name, endTime, (endTime - startTime));
        }
        return obj;
    }
}
