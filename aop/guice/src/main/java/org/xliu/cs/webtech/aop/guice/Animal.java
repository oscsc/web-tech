package org.xliu.cs.webtech.aop.guice;

public interface Animal {
    void eat();
}
