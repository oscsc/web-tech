package org.xliu.cs.webtech.aop.guice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CatTest {

    @Test
    public void testInjectSingleton() {
        // 单例
        assertSame(GuiceInjector.injector.getInstance(Animal.class), GuiceInjector.injector.getInstance(Animal.class));
    }

}