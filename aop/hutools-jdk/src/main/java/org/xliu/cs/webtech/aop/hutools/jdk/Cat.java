package org.xliu.cs.webtech.aop.hutools.jdk;

import cn.hutool.aop.ProxyUtil;
import cn.hutool.aop.aspects.SimpleAspect;
import cn.hutool.core.lang.Console;

import java.lang.reflect.Method;

// JDK代理实现AOP，需要定义父接口
public class Cat implements Animal {

    @Override
    public void eat() {
        Console.log("猫吃鱼");
        throw new RuntimeException();
    }

    static class Aop extends SimpleAspect {
        @Override
        public boolean before(Object target, Method method, Object[] args) {
            Console.log("before call {} method {} ", target, method);
            return true;
        }

        @Override
        public boolean after(Object target, Method method, Object[] args, Object returnVal) {
            Console.log("after call {} method {} ", target, method);
            return true;
        }

        @Override
        public boolean afterException(Object target, Method method, Object[] args, Throwable e) {
            Console.log("after exception call {} method {} ", target, method);
            return true;
        }
    }

    public static void main(String[] args) {
        // 不引入cglib包，SPI机制只能加载最后一个 JdkProxyFactory

        // 要用父类声明，否则会报错
        // Cat cat = ProxyUtil.proxy(new Cat(), Aop.class);

        Animal cat = ProxyUtil.proxy(new Cat(), Aop.class);
        Class<? extends Animal> aClass = cat.getClass();
        // 只是 Animal 的子类，因此不能 cast 为 Cat 实例
        Console.log(aClass + ", " + aClass.getInterfaces()[0]);

        try {
            cat.eat();
        } catch (Exception e) {
            Console.log("handle exception!");
        }
    }
}