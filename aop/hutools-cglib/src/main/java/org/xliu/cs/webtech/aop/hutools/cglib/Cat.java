package org.xliu.cs.webtech.aop.hutools.cglib;

import cn.hutool.aop.ProxyUtil;
import cn.hutool.aop.aspects.SimpleAspect;
import cn.hutool.core.lang.Console;

import java.lang.reflect.Method;

// Cglib实现AOP，无需父接口
public class Cat{

    public void eat() {
        Console.log("猫吃鱼");
    }

    static class Aop extends SimpleAspect {
        @Override
        public boolean before(Object target, Method method, Object[] args) {
            Console.log("before call {} method {} ", target, method);
            return true;
        }

        @Override
        public boolean after(Object target, Method method, Object[] args, Object returnVal) {
            Console.log("after call {} method {} ", target, method);
            return true;
        }
    }

    public static void main(String[] args) {
        // 引入cglib包，使用 CglibProxyFactory
        Cat cat = ProxyUtil.proxy(new Cat(), Aop.class);
        cat.eat();
    }
}