package org.xliu.cs.webtech.fsm.stateless4j;

public enum Event {
    PREPARE, RUN, SUCCESS, FAIL;
}
