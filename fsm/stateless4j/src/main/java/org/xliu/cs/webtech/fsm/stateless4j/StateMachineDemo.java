package org.xliu.cs.webtech.fsm.stateless4j;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.StateMachineConfig;

public class StateMachineDemo {

    private static void init(StateMachineConfig<State, Event> config) {
        /*
         * INIT 状态，允许的事件，以及后续状态
         */
        config.configure(State.INIT)
                .permit(Event.PREPARE, State.PENDING)
                // 动态的目标的State
                // .permitDynamic()
                // 忽略事件
                .ignore(Event.SUCCESS);

        config.configure(State.PENDING)
                .permit(Event.RUN, State.RUNNING)
                .onEntryFrom(Event.PREPARE, arg1 -> {
                    System.out.println(arg1.getDestination() + " is from " + arg1.getSource() + " by " + arg1.getTrigger() );
                })
                .ignore(Event.PREPARE);

        config.configure(State.RUNNING)
                .permit(Event.SUCCESS, State.SUCCEED)
                .permit(Event.FAIL, State.FAILED)
                .ignore(Event.PREPARE);
    }

    public static StateMachine<State, Event> of(State sourceState, StateMachineConfig<State, Event> config) {
        return new StateMachine<>(sourceState, config);
    }

    public static void main(String[] args) {
        StateMachineConfig<State, Event> config = new StateMachineConfig<>();
        init(config);
        //构建状态机实例，传入当前状态和状态转移配置
        StateMachine<State, Event> sm = of(State.INIT, config);
        sm.onUnhandledTrigger((state, trigger) -> {
            System.out.println("illegal state transform: " + state + " - " + trigger);
        });

        // 判断当前状态能不能接受目标事件: 提交质检结果
        if (sm.canFire(Event.SUCCESS)) {
            // 中断
            System.out.println("不能处理事件" + Event.SUCCESS);
        }

        //接受事件，状态转移到下一个状态
        sm.fire(Event.PREPARE);

        // 错误事件，但配置了ignore，不会打印
        sm.fire(Event.PREPARE);

        // 错误事件，未配置 ignore，会打印
        sm.fire(Event.SUCCESS);

        //更新表里的状态
        State nextState = sm.getState(); //推荐这样获取下一个状态，减少硬编码

        System.out.println("current state is " + nextState);
    }
}
