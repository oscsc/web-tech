package org.xliu.cs.webtech.fsm.stateless4j;

public enum State {
    RUNNING, PENDING, SUCCEED, FAILED, INIT
}
