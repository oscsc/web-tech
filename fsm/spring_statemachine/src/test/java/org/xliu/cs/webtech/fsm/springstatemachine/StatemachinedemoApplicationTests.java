package org.xliu.cs.webtech.fsm.springstatemachine;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import reactor.core.publisher.Mono;

@SpringBootTest
class StatemachinedemoApplicationTests {

    @Autowired
    StateMachineFactory<State, Event> factory;

    @Test
    void contextLoads() {
        StateMachine<State, Event> stateMachine = factory.getStateMachine("id");

        System.out.println("machine id is :  " + stateMachine.getId());

        stateMachine.startReactively().block();
        // Note: 这一步不合符状态机，没有执行
        stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(Event.RUN)
                .setHeader("key", "no valid")
                .build())).blockFirst();

        stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(Event.PREPARE)
                .setHeader("key", "value")
                .build())).blockFirst();

        stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(Event.RUN)
                .setHeader("key", "run")
                .build())).blockFirst();
    }


}