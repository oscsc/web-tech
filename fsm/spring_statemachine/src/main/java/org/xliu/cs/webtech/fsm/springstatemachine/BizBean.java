package org.xliu.cs.webtech.fsm.springstatemachine;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;

@WithStateMachine
@Data
@Slf4j
public class BizBean {

    /**
     * @see State
     */
    private String status = State.INITIAL.name();

    // TODO: 如何避免多次的字符常量
    @OnTransition(target = "PENDING")
    public void prepare(Message<Event> message) {
        log.info("准备操作. target status:{}, key is {}", State.PENDING.name(), message.getHeaders().get("key"));
        setStatus(State.PENDING.name());
    }

    @OnTransition(target = "RUNNING")
    public void run() {
        log.info("运行操作. target status:{}", State.RUNNING.name());
        setStatus(State.RUNNING.name());
    }

    @OnTransition(target = "SUCCEED")
    public void succeed() {
        log.info("成功操作. target status:{}", State.SUCCEED.name());
        setStatus(State.SUCCEED.name());
    }


    @OnTransition(target = "FAILED")
    public void fail() {
        log.info("失败操作. target status:{}", State.FAILED.name());
        setStatus(State.FAILED.name());
    }

}