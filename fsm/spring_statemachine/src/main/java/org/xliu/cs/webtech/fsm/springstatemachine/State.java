package org.xliu.cs.webtech.fsm.springstatemachine;

public enum State {
    INITIAL,
    PENDING,
    RUNNING,
    SUCCEED,
    FAILED;
}
