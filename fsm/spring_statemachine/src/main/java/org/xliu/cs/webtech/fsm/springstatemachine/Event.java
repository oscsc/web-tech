package org.xliu.cs.webtech.fsm.springstatemachine;

public enum Event {
    PREPARE,
    RUN,
    SUCCESS,
    FAIL;
}
