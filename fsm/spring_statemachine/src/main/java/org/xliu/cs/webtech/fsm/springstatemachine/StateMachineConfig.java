package org.xliu.cs.webtech.fsm.springstatemachine;

import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

@Configuration
@EnableStateMachineFactory
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<State, Event> {

    @Override
    public void configure(StateMachineStateConfigurer<State, Event> states) throws Exception {
        states.withStates().initial(State.INITIAL).states(EnumSet.allOf(State.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<State, Event> transitions) throws Exception {
        transitions.withExternal()
                .source(State.INITIAL).target(State.PENDING)
                .event(Event.PREPARE)
                .and()
                .withExternal()
                .source(State.PENDING).target(State.RUNNING)
                .event(Event.RUN)
                .and()
                .withExternal()
                .source(State.RUNNING).target(State.SUCCEED)
                .event(Event.SUCCESS)
                .and()
                .withExternal()
                .source(State.RUNNING).target(State.FAILED)
                .event(Event.FAIL);
    }
}