package org.xliu.cs.webtech.fsm.squirrel;

import org.squirrelframework.foundation.fsm.Action;
import org.squirrelframework.foundation.fsm.StateMachine;
import org.squirrelframework.foundation.fsm.StateMachineBuilder;
import org.squirrelframework.foundation.fsm.StateMachineBuilderFactory;
import org.squirrelframework.foundation.fsm.impl.AbstractStateMachine;

public class QuickStartSample {
    // 1. Define State Machine Event
    enum Event {
        PREPARE, RUN, SUCCESS, FAIL;
    }

    enum State {
        RUNNING, PENDING, SUCCEED, FAILED, INIT
    }

    static class ActionAdapter<T extends StateMachine<T, S, E, C>, S, E, C> implements Action<T, S, E, C> {
        @Override
        public void execute(S from, S to, E event, C context, T stateMachine) {

        }

        @Override
        public String name() {
            return null;
        }

        @Override
        public int weight() {
            return 0;
        }

        @Override
        public boolean isAsync() {
            return false;
        }

        @Override
        public long timeout() {
            return 0;
        }
    }

    // 2. Define State Machine Class
    static class StateMachineSample extends AbstractStateMachine<StateMachineSample, State, Event, Integer> {

        @Override
        protected void afterTransitionCausedException(State fromState, State toState, Event event, Integer context) {
            System.out.println("afterTransitionCausedException: from '" + fromState + "' to '" + toState + "' on event '" + event +
                                       "' with context '" + context + "'." + getLastException());
        }

        @Override
        protected void beforeTransitionBegin(State fromState, Event event, Integer context) {
            System.out.println("beforeTransitionBegin: from '" + fromState + " on event '" + event +
                                       "' with context '" + context + "'.");
        }

        @Override
        protected void afterTransitionCompleted(State fromState, State toState, Event event, Integer context) {
            System.out.println("afterTransitionCompleted: from '" + fromState + "' to '" + toState + "' on event '" + event +
                                       "' with context '" + context + "'.");
        }

        @Override
        protected void afterTransitionEnd(State fromState, State toState, Event event, Integer context) {
            System.out.println("afterTransitionEnd: from '" + fromState + "' to '" + toState + "' on event '" + event +
                                       "' with context '" + context + "'.");
        }

        @Override
        protected void afterTransitionDeclined(State fromState, Event event, Integer context) {
            System.out.println("afterTransitionDeclined: from '" + fromState + " on event '" + event +
                                       "' with context '" + context + "'.");
        }

        @Override
        protected void beforeActionInvoked(State fromState, State toState, Event event, Integer context) {
            System.out.println("BeforeAction: from '" + fromState + "' to '" + toState + "' on event '" + event +
                                       "' with context '" + context + "'.");
        }

        @Override
        protected void afterActionInvoked(State fromState, State toState, Event event, Integer context) {
            System.out.println("AfterAction: from '" + fromState + "' to '" + toState + "' on event '" + event +
                                       "' with context '" + context + "'.");
        }
    }

    public static void main(String[] args) {

        // 3. Build State Transitions
        StateMachineBuilder<StateMachineSample, State, Event, Integer> builder =
                StateMachineBuilderFactory.create(StateMachineSample.class, State.class, Event.class, Integer.class);

        builder.transition().from(State.INIT).to(State.PENDING).on(Event.PREPARE)
                .perform(new ActionAdapter<StateMachineSample, State, Event, Integer>() {
                    @Override
                    public void execute(State from, State to, Event event, Integer context, StateMachineSample stateMachine) {
                        System.out.println(Thread.currentThread() + "Transition. from '" + from + "' to '" + to + "' on event '" + event +
                                                   "' with context '" + context + "'.");
                    }
                });

        builder.transition().from(State.PENDING).to(State.RUNNING).on(Event.RUN);
        builder.transition().from(State.RUNNING).to(State.SUCCEED).on(Event.SUCCESS);
        builder.transition().from(State.RUNNING).to(State.FAILED).on(Event.FAIL);

        builder.onEntry(State.RUNNING).perform(new ActionAdapter<StateMachineSample, State, Event, Integer>() {
            @Override
            public void execute(State from, State to, Event event, Integer context, StateMachineSample stateMachine) {
                // Note: 对于 Entry 的场景，其 from 为 NULL
                System.out.println(Thread.currentThread() + "Entry. from '" + from + "' to '" + to + "' on event '" + event +
                                           "' with context '" + context + "'.");
            }
        });
        builder.onExit(State.RUNNING).perform(new ActionAdapter<StateMachineSample, State, Event, Integer>() {
            @Override
            public void execute(State from, State to, Event event, Integer context, StateMachineSample stateMachine) {
                // Note: 对于 Exit 的场景，其 to 为 NULL
                System.out.println(Thread.currentThread() + "Exit. from '" + from + "' to '" + to + "' on event '" + event +
                                           "' with context '" + context + "'.");
            }
        });

        // 4. Use State Machine
        StateMachine<StateMachineSample, State, Event, Integer> fsm = builder.newStateMachine(State.INIT);

        fsm.fire(Event.PREPARE, 10);
        fsm.fire(Event.RUN, 100);
        fsm.fire(Event.SUCCESS, 1000);
        // 不执行
        fsm.fire(Event.FAIL, 10000);

        System.out.println("Current state is " + fsm.getCurrentState());
    }
}
