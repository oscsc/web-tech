package org.xliu.cs.webtech.reactor;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalTime;

public class ReactorUsage {


    public static void testNoArgSubscribe() {
        Mono<Integer> callMono = Mono.<Integer>create(sink -> {
            System.out.println("testNoArgSubscribe");
            sink.success(1);
        });

        callMono.subscribe();
    }

    public static void create() {
        Mono<String> helloWorld = Mono.just("Hello World");

        Flux<String> fewWords = Flux.just("Hello", "World");
        fewWords.subscribe();

        Mono<Integer> callMono = Mono.<Integer>create(sink -> {
            System.out.println("callable before sleep:" + LocalTime.now());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                sink.error(new RuntimeException(e));
                return;
            }
            System.out.println("callable after sleep:" + LocalTime.now());

            sink.success(1);
        });

        callMono.subscribe(k -> {
            System.out.println("subscribe callable :" + LocalTime.now());
        });
    }

    public static Mono<Void> complicatedLogic() {
        // 当 status == 0 时，继续处理
        Mono<Integer> status = Mono.just(0);
        // 如果获取Mono的时候有异常，后续无处理时，直接抛出
//        Mono<Integer> status = Mono.error(new RuntimeException("can not get status"));

        return status
                .flatMap(s -> {
                    System.out.println("handle status");
                    if (s != 0) {
                        return Mono.error(new RuntimeException("status is not illegal"));
                    }

                    return Mono.empty();
                })
                .then(Mono.just(0)
                        .flatMap(s -> {
                            System.out.println("handle right");
                            if (s != 0) {
                                return Mono.error(new RuntimeException("token is expired"));
                            }

                            return Mono.empty();
                        }));

    }


    public static void main(String[] args) {
        testNoArgSubscribe();

        complicatedLogic().subscribe();
    }
}
