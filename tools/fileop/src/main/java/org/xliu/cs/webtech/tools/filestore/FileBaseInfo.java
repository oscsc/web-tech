package org.xliu.cs.webtech.tools.filestore;

import java.io.InputStream;
import lombok.Data;

@Data
public class FileBaseInfo {
    private InputStream inputStream;
    private String fileName;
    private Long streamSize;

    public FileBaseInfo(InputStream inputStream, String fileName, Long streamSize) {
        this.inputStream = inputStream;
        this.fileName = fileName;
        this.streamSize = streamSize;
    }
}
