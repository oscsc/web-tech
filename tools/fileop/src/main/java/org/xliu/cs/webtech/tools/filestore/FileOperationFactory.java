package org.xliu.cs.webtech.tools.filestore;


import org.xliu.cs.webtech.tools.filestore.fuse.LocalFsOp;
import org.xliu.cs.webtech.tools.filestore.minio.MinIOOp;

public class FileOperationFactory {
    private FileOperationFactory() {}


    public static FileOperation getLocalFsOp(String basePath) {
        return new LocalFsOp(basePath);
    }

    public static FileOperation getMinIOOp(String url, String username, String password) {
        return new MinIOOp(url, username, password);
    }
}
