package org.xliu.cs.webtech.tools.filestore;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 文件操作接口
 */
public interface FileOperation {

    /**
     * 获取文件路径
     */
    default String getFilePath(String... dirs) {
        StringBuilder result = new StringBuilder();
        for (String dir : dirs) {
            result.append(File.separator).append(dir);
        }
        return result.substring(File.separator.length());
    }


    /**
     * 创建目录本地文件需要创建路径，对象存储需要创建桶
     */
    void createCategory(String dirOrBucketName) throws IOException;


    /**
     * 开始分片文件上传.
     *
     * @param dirOrBucketName 桶名(s3) 或 父目录
     * @param relativePath    相对于桶名或父目录的路径
     * @return uploadId  id，用于 {@link FileOperation#uploadMultiPartFile}的 uploadId
     */
    String createMultipartUpload(String dirOrBucketName, String relativePath) throws IOException;

    /**
     * 上传分片.
     *
     * @param dirOrBucketName  桶名
     * @param path             路径名
     * @param inputStream      数据流（调用方负责关闭）
     * @param uploadId         分片上传的Id
     * @param currentChunkSize 当前分片大小
     * @param chunkNumber      当前分片数
     * @param totalChunks      总的分片数
     */
    void uploadMultiPartFile(String dirOrBucketName, String path, InputStream inputStream, String uploadId,
                             int currentChunkSize, int chunkNumber, int totalChunks) throws IOException;

    /**
     * 持久化临时文件
     *
     * @param dirOrBucketName
     * @param relativePath
     * @return 文件路径
     */
    String persistentFile(String dirOrBucketName, String relativePath) throws IOException;

    /**
     * 删除文件
     *
     * @param dirOrBucketName
     * @param relativePath
     * @return
     */
    void deleteFile(String dirOrBucketName, String relativePath) throws IOException;

    /**
     * 获取文件大小
     *
     * @param dirOrBucketName
     * @param relativePath
     * @return
     */
    Long getFileSize(String dirOrBucketName, String relativePath) throws IOException;

    /**
     * 获取完整文件流
     *
     * @param dirOrBucketName
     * @param relativePath
     * @return
     */
    FileBaseInfo getInputStream(String dirOrBucketName, String relativePath) throws IOException;

    /**
     * 获取指定范围的文件流
     *
     * @param dirOrBucketName
     * @param path
     * @param offSet
     * @param length
     * @return
     */
    FileBaseInfo getInputStreamByRange(String dirOrBucketName, String path, Long offSet, Long length) throws IOException;

    /**
     * 获取文件夹的文件名列表
     *
     * @param path
     * @param limit
     * @return
     */
    List<String> listFileNames(String dirOrBucketName, String path, int limit);

}
