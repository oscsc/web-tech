package org.xliu.cs.webtech.tools.filestore.minio;

import org.xliu.cs.webtech.tools.filestore.FileBaseInfo;
import org.xliu.cs.webtech.tools.filestore.FileOperation;
import io.minio.*;
import io.minio.messages.Part;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.List;

/**
 * MINIO 的文件操作
 */
public class MinIOOp implements FileOperation {

    private final EnhancedMinioClient client;

    private static final Integer PARTS_NUM_MAX = 10000;

    public MinIOOp(String url, String username, String password) {
        MinioClient minioClient = MinioClient.builder()
                .endpoint(url)
                .credentials(username, password)
                .build();
        client = new EnhancedMinioClient(minioClient);
    }

    /**
     * 创建 bucket
     *
     * @param bucket 桶名
     */
    @Override
    public void createCategory(String bucket) throws IOException {
        try {
            if (!client.bucketExists(BucketExistsArgs.builder().bucket(bucket).build())) {
                client.makeBucket(MakeBucketArgs.builder().bucket(bucket).build());
            }
        } catch (Exception e) {
            throw new IOException("创建桶失败!", e);
        }
    }

    /**
     * 上传单个文件，不能超过 5G（超过应该使用分片上传）.
     *
     * @param bucket      桶名
     * @param path        桶下的路径
     * @param inputStream 数据流
     */
    public void uploadFile(String bucket, String path, InputStream inputStream) throws IOException {
        try {
            client.putObject(PutObjectArgs.builder()
                    .bucket(bucket)
                    .object(path)
                    .stream(inputStream, inputStream.available(), -1)
                    .build());
        } catch (Exception e) {
            throw new IOException("文件上传异常!", e);
        }
    }

    private void uploadTempFile(String category, String path, InputStream inputStream) throws IOException {
        try {
            // 检查存储桶是否已经存在
            boolean isExist = client.bucketExists(BucketExistsArgs.builder().bucket(category).build());
            if (!isExist) {
                createCategory(category);
            }
            client.putObject(PutObjectArgs.builder()
                    .bucket(category)
                    .object(path)
                    .stream(inputStream, inputStream.available(), 20 * 1024 * 1024)
                    .build());
        } catch (Exception e) {
            throw new IOException("文件上传异常!", e);
        }
    }

    @Override
    public void deleteFile(String bucket, String path) throws IOException {
        try {
            client.removeObject(RemoveObjectArgs.builder().bucket(bucket).object(path).build());
        } catch (Exception e) {
            throw new IOException("文件删除异常!", e);
        }
    }

    @Override
    public String createMultipartUpload(String bucket, String relativePath) throws IOException {
        try {
            // 检查存储桶是否已经存在
            boolean isExist = client.bucketExists(BucketExistsArgs.builder().bucket(bucket).build());
            if (!isExist) {
                createCategory(bucket);
            }
            // 获取uploadId
            CreateMultipartUploadResponse response = client.createMultipartUpload(bucket, null, relativePath, null, null);
            return response.result().uploadId();
        } catch (Exception e) {
            throw new IOException("文件上传异常!", e);
        }
    }

    @Override
    public void uploadMultiPartFile(String bucket, String path, InputStream inputStream, String uploadId,
                                    int currentChunkSize, int chunkNumber, int totalChunks) throws IOException {
        if (uploadId == null || uploadId.length() == 0 || totalChunks > PARTS_NUM_MAX) {
            throw new IllegalArgumentException("分片参数异常!", null);
        }
        try {
            client.uploadPart(bucket, null, path, inputStream, currentChunkSize, uploadId, chunkNumber, null, null);

            // 最后一个分片
            if (chunkNumber == totalChunks) {
                // the complete cost little time (0.374 s for 200MB)
                Part[] parts = new Part[PARTS_NUM_MAX];
                ListPartsResponse partResult = client.listMultipart(bucket, null, path, PARTS_NUM_MAX, 0, uploadId, null, null);
                int partNumber = 1;
                for (Part part : partResult.result().partList()) {
                    parts[partNumber - 1] = new Part(partNumber, part.etag());
                    partNumber++;
                }
                client.completeMultipartUpload(bucket, null, path, uploadId, parts, null, null);
            }
        } catch (Exception e) {
            throw new IOException("上传文件失败", e);
        }
    }

    @Override
    public String persistentFile(String bucket, String relativePath) throws IOException {
        try {
            //            Retention retention =
            //                new Retention(RetentionMode.GOVERNANCE, ZonedDateTime.now().plusYears(100));
            //            client.setObjectRetention(
            //                SetObjectRetentionArgs.builder()
            //                    .bucket(category)
            //                    .object(relativePath)
            //                    .config(retention)
            //                    .bypassGovernanceMode(true)
            //                    .build());

        } catch (Exception e) {
            throw new IOException("保存文件异常!", e);
        }

        return relativePath;
    }

    @Override
    public Long getFileSize(String bucket, String relativePath) throws IOException {
        try {
            StatObjectResponse statObjectResponse = client.statObject(StatObjectArgs.builder()
                    .bucket(bucket)
                    .object(relativePath)
                    .build());

            return statObjectResponse.size();
        } catch (Exception e) {
            throw new IOException("保存文件异常!", e);
        }
    }

    @Override
    public FileBaseInfo getInputStream(String category, String path) throws IOException {
        try {
            GetObjectResponse response = client.getObject(GetObjectArgs.builder()
                    .bucket(category)
                    .object(path)
                    .build());
            return new FileBaseInfo(response, response.object(),
                    Long.parseLong(response.headers().get("Content-Length")));
        } catch (Exception e) {
            throw new IOException("获取文件异常!", e);
        }
    }

    @Override
    public FileBaseInfo getInputStreamByRange(String category, String path, Long offSet, Long length) throws IOException {
        try {
            GetObjectResponse response = client.getObject(GetObjectArgs.builder()
                    .bucket(category)
                    .object(path)
                    .offset(offSet)
                    .length(length)
                    .build());
            return new FileBaseInfo(response, response.object(),
                    Long.parseLong(response.headers().get("Content-Length")));
        } catch (Exception e) {
            throw new IOException("获取文件异常!", e);
        }
    }

    @Override
    public List<String> listFileNames(String category, String path, int limit) {
        throw new InvalidParameterException("该接口暂未使用");
    }

}