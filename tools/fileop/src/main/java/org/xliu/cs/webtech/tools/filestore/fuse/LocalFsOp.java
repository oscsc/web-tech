package org.xliu.cs.webtech.tools.filestore.fuse;

import org.apache.commons.io.FileUtils;
import org.xliu.cs.webtech.tools.filestore.FileBaseInfo;
import org.xliu.cs.webtech.tools.filestore.FileOperation;

import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class LocalFsOp implements FileOperation {

    public final String basePath;
    /**
     * The default buffer size used when copying bytes.
     */
    public static final int BUFFER_SIZE = 4096;

    public LocalFsOp(String basePath) {
        this.basePath = basePath;
    }

    @Override
    public void createCategory(String dirOrBucketName) throws IOException {
        File file = new File(getFilePath(basePath, dirOrBucketName));
        if (file.exists() && file.isFile()) {
            throw new IllegalArgumentException("路径已存在，且不是目录!", null);
        }
        Files.createDirectories(file.toPath());
    }

    public void downloadFile(String category, String relativePath) {
        File file = new File(getFilePath(basePath, category, relativePath));
        if (!file.exists()) {
            throw new IllegalArgumentException("路径不存在！", null);
        }
//        try {
//            FileUtil.downloadByStream(new FileInputStream(file), response, file.getName(), file.length());
//        } catch (Exception e) {
//            log.error("LocalFsOp downloadFile occurs exception: ", e);
//            throw new ServerException("服务下载异常！", e);
//        }
    }


    public void uploadTempFile(String category, String relativePath, InputStream in) throws IOException {
        File dest = new File(getTempFilePath(category, relativePath));
        if (!dest.getParentFile().exists()) {
            //不存在就创建一个
            dest.getParentFile().mkdirs();
        }
        try (OutputStream out = Files.newOutputStream(dest.toPath())) {
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
        } catch (IOException e) {
            throw new IOException("上传文件失败", e);
        }
    }

    @Override
    public void deleteFile(String category, String relativePath) throws IOException {
        File file = new File(getFilePath(basePath, category, relativePath));
        if (file.exists()) {
            try {
                if (file.isFile()) {
                    Files.delete(file.toPath());
                } else if (file.isDirectory()) {
                    FileUtils.deleteDirectory(file);
                }
            } catch (IOException e) {
                throw new IOException("删除文件失败", null);
            }
        }
    }

    @Override
    public String createMultipartUpload(String category, String relativePath) {
        String uploadId = UUID.randomUUID().toString();
        File parentFileDir = new File(getTempDirPath(category, relativePath, uploadId));
        if (!parentFileDir.exists()) {
            parentFileDir.mkdirs();
        }
        return uploadId;
    }

    @Override
    public void uploadMultiPartFile(String category, String path, InputStream inputStream, String uploadId,
                                    int currentChunkSize, int chunkNumber, int totalChunks) throws IOException {
        if (totalChunks == 1) {
            uploadTempFile(category, path, inputStream);
        } else {
            uploadMultipartTempFile(category, path, inputStream, uploadId, chunkNumber);
        }
    }

    @Override
    public String persistentFile(String category, String relativePath) {
        //临时文件
        File destTempFile = new File(getTempFilePath(category, relativePath));
        //最终文件
        File destFile = new File(getFilePath(basePath, category, relativePath));
        if (!destFile.getParentFile().exists()) {
            //不存在就创建一个
            destFile.getParentFile().mkdirs();
        }
        destTempFile.renameTo(destFile);
        return getFilePath(category, relativePath);
    }


    public void uploadMultipartTempFile(String category, String path, InputStream in, String uploadId, Integer partNumber) throws IOException {
        try {
            //临时文件
            File destTempFile = new File(getTempFilePath(category, path));
            if (!destTempFile.exists()) {
                //先得到文件的上级目录，并创建上级目录，在创建文件
                destTempFile.getParentFile().mkdir();
                destTempFile.createNewFile(); //上级目录没有创建，这里会报错
            }

            try (FileOutputStream os = new FileOutputStream(destTempFile, true)) {
                int len = -1;
                byte[] bytes = new byte[1024];
                while ((len = in.read(bytes)) != -1) {
                    os.write(bytes, 0, len);
                }
            }
        } catch (Exception e) {
            throw new IOException("上传文件失败", e);
        }
    }


    @Override
    public Long getFileSize(String category, String relativePath) {
        File file = new File(getFilePath(basePath, category, relativePath));
        return FileUtils.sizeOf(file);
    }

    @Override
    public FileBaseInfo getInputStream(String category, String relativePath) {
        File file = new File(getFilePath(basePath, category, relativePath));
        if (!file.exists()) {
            throw new IllegalArgumentException("路径不存在！", null);
        }
        try {
            return new FileBaseInfo(new FileInputStream(file), file.getName(), file.length());
        } catch (Exception e) {
            throw new IllegalArgumentException("获取文件错误", e);
        }
    }

    @Override
    public FileBaseInfo getInputStreamByRange(String category, String path, Long offSet, Long length) {
        throw new IllegalArgumentException("该接口暂未使用", null);
    }

    @Override
    public List<String> listFileNames(String category, String path, int limit) {
        File dir = new File(getFilePath(basePath, category, path));
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException(String.format("文件目录错误:%s", dir.getPath()));
        }
        List<File> files = Arrays.stream(dir.listFiles()).limit(limit).collect(Collectors.toList());
        return files.stream().map(File::getName).collect(Collectors.toList());
    }

    private String getTempDirPath(String category, String relativePath, String uploadId) {
        String temp = ".temp";
        return getFilePath(basePath, temp, "." + relativePath + "_" + uploadId);
    }

    private String getTempFilePath(String category, String relativePath) {
        String temp = ".temp";
        return getFilePath(basePath, temp, "." + relativePath);
    }

}
