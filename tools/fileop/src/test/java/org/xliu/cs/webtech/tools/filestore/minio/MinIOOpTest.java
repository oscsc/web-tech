package org.xliu.cs.webtech.tools.filestore.minio;

import org.xliu.cs.webtech.tools.filestore.FileOperation;
import org.xliu.cs.webtech.tools.filestore.FileOperationFactory;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;

class MinIOOpTest {

    @org.junit.jupiter.api.Test
    @EnabledIfEnvironmentVariable(named = "TestMinIO", matches = "true")
    void createTempFileUpload() throws Exception {
        String minioAddr = "http://172.16.2.132:32129";
        String username = "minio";
        String passwd = "minio123";
        String filePath = "E:\\NVIDIA-Turing-Architecture-Whitepaper.pdf";

        FileOperation op = FileOperationFactory.getMinIOOp(minioAddr, username, passwd);

        File file = new File(filePath);
        InputStream in = Files.newInputStream(file.toPath());

        int chunkSize = 10 * 1024 * 1024;
        int totalChunks = (int) Math.ceil(file.length() * 1.0 / chunkSize);

        long start = System.currentTimeMillis();

        String uploadId = op.createMultipartUpload("test", "bigfile/exe");

        for (int chunkNumber = 1; chunkNumber <= totalChunks; chunkNumber++) {
            int currentChunkSize = chunkSize;
            if (chunkNumber == totalChunks) {
                currentChunkSize = (int) (file.length() - chunkSize * (chunkNumber - 1));
            }
            System.out.println("Chunk Size is " + chunkNumber + ": " + currentChunkSize);
            op.uploadMultiPartFile("test", "bigfile/exe", in, uploadId,
                    currentChunkSize, chunkNumber, totalChunks);
        }

        System.out.println("Cost: " + (System.currentTimeMillis() - start) / 1000.0 + " s");

    }


    @org.junit.jupiter.api.Test
    @EnabledIfEnvironmentVariable(named = "TestMinIO", matches = "true")
    void uploadFile() throws Exception {
        String minioAddr = "http://172.16.2.132:32129";
        String username = "minio";
        String passwd = "minio123";
        String filePath = "D:\\安装包\\postgresql-12.2-2-windows-x64.exe";

        MinIOOp op = (MinIOOp) FileOperationFactory.getMinIOOp(minioAddr, username, passwd);

        File file = new File(filePath);
        InputStream in = Files.newInputStream(file.toPath());

        long start = System.currentTimeMillis();

        op.uploadFile("test", "bigfile/exeonce", in);

        // 200M 183.758 s
        System.out.println("Cost: " + (System.currentTimeMillis() - start) / 1000.0 + " s");
    }
}