package org.xliu.cs.webtech.oplog.bussiness;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.stereotype.Component;
import org.xliu.cs.webtech.oplog.bussiness.func.ParseFunctionFactory;
import org.xliu.cs.webtech.oplog.bussiness.io.Result;
import org.xliu.cs.webtech.oplog.bussiness.log.ILogRecordService;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Aspect
@Component
@Slf4j
public class LogRecordInterceptorAspect {

    private final LogRecordExpressionEvaluator evaluator = new LogRecordExpressionEvaluator();

    private final ParseFunctionFactory functionFactory;

    private final ILogRecordService logRecordService;


    // {CUSTOMIZED_FUNCTION{#SPEL}}
    private static final Pattern pattern = Pattern.compile("\\{\\s*(\\w*)\\s*\\{(.*?)}}");

    public LogRecordInterceptorAspect(ParseFunctionFactory functionFactory, ILogRecordService logRecordService) {
        this.functionFactory = functionFactory;
        this.logRecordService = logRecordService;
    }

    /**
     * 解析 这个 method 上有没有 @LogRecordAnnotation 注解，有的话会解析出来注解上的各个参数
     */
    private LogRecordOp computeLogRecordOperations(Method method) {
        LogRecordAnnotation annotation = method.getAnnotation(LogRecordAnnotation.class);
        if (annotation == null) {
            return null;
        }

        return new LogRecordOp(annotation.value(), LogRecordContext.getUserId(), annotation.bizNo());
    }

    @Around("@annotation(org.xliu.cs.webtech.oplog.bussiness.LogRecordAnnotation)")
    private Object execute(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        // 获取方法对象
        MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = signature.getMethod();

        Object ret;

        // 新建栈
        LogRecordContext.putEmptySpan();

        // TODO: 全局的用户信息，应该在 interceptor 处进行设置（如通过 token 获取等)
        LogRecordContext.setUserId(123L);

        LogRecordOp logRecordOp = computeLogRecordOperations(method);

        // do before function.
        Map<String, String> functionNameAndReturnMap = new HashMap<>();
        try {
            if (logRecordOp != null) {
                // 获取每个函数，及其之心结果
                functionNameAndReturnMap = processBeforeExecuteFunctionTemplate(logRecordOp, method.getClass(), method, proceedingJoinPoint.getArgs());
            }
        } catch (Exception e) {
            log.error("log record parse before function exception", e);
        }

        // 执行原方法的逻辑
        // TODO: 是否需要记录执行结果/异常
        try {
            ret = proceedingJoinPoint.proceed();
        } finally {
            try {
                if (logRecordOp != null) {
                    recordExecute(method, proceedingJoinPoint.getArgs(), logRecordOp, method.getClass(), functionNameAndReturnMap);
                }
            } catch (Exception t) {
                //记录日志错误不要影响业务
                log.error("log record parse exception", t);
            } finally {
                LogRecordContext.clear();
            }
        }
        return ret;
    }


    private Map<String, String> processBeforeExecuteFunctionTemplate(LogRecordOp logRecordOp, Class<?> targetClass, Method method, Object[] args) {
        String spElTemplate = logRecordOp.getContent();
        LogRecordEvaluationContext context = new LogRecordEvaluationContext(method, args, new DefaultParameterNameDiscoverer());
        // 函数名以及返回值
        Map<String, String> functionNameAndReturnValueMap = new HashMap<>();

        if (spElTemplate.contains("{")) {
            Matcher matcher = pattern.matcher(spElTemplate);
            while (matcher.find()) {
                String expression = matcher.group(2);
                AnnotatedElementKey annotatedElementKey = new AnnotatedElementKey(method, targetClass);
                String functionName = matcher.group(1);
                if (functionFactory.isBeforeFunction(functionName)) {
                    String value = evaluator.parseExpression(expression, annotatedElementKey, context);
                    String functionReturnValue = functionFactory.getFunctionReturnValue(null, value, expression, functionName);
                    String functionCallInstanceKey = functionFactory.getFunctionCallInstanceKey(functionName, expression);
                    functionNameAndReturnValueMap.put(functionCallInstanceKey, functionReturnValue);
                }
            }
        }

        return functionNameAndReturnValueMap;

    }

    private void recordExecute(Method method, Object[] args, LogRecordOp logRecordOp, Class<?> targetClass, Map<String, String> beforeFunctionAndValue) {
        String spElTemplate = logRecordOp.getContent();
        LogRecordEvaluationContext context = new LogRecordEvaluationContext(method, args, new DefaultParameterNameDiscoverer());

        // 可用使用 Matcher 的 appendReplacement(JDK 9)
        StringBuilder result = new StringBuilder();

        // {CUSTOMIZED_FUNCTION{#SPEL}}

        if (!spElTemplate.contains("{")) {
            logRecordService.record(logRecordOp);
            return;
        }

        int begin = 0;
        Matcher matcher = pattern.matcher(spElTemplate);
        while (matcher.find()) {
            result.append(spElTemplate, begin, matcher.start());

            String expression = matcher.group(2);
            AnnotatedElementKey annotatedElementKey = new AnnotatedElementKey(method, targetClass);
            String functionName = matcher.group(1);

            // TODO: 避免重算
            String value = evaluator.parseExpression(expression, annotatedElementKey, context);

            String functionReturnValue = functionFactory.getFunctionReturnValue(beforeFunctionAndValue, value, expression, functionName);
            String functionCallInstanceKey = functionFactory.getFunctionCallInstanceKey(functionName, expression);

            // 重复表达式
            beforeFunctionAndValue.put(functionCallInstanceKey, functionReturnValue);

            result.append(functionReturnValue);
            begin = matcher.end();  // no need +1

        }
        result.append(spElTemplate, begin, spElTemplate.length());

        // TODO: 支持  bizNo 等其他字段支持表达式，可以通过 map 存储原始和解析后的表达式，然后进行覆盖。
        logRecordOp.setContent(result.toString());
        logRecordService.record(logRecordOp);
    }
}
