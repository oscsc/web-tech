package org.xliu.cs.webtech.oplog.bussiness.io;

import com.fasterxml.jackson.annotation.JsonValue;


public enum CodeNumEnum {
    //操作成功
    SUCCESS(0),
    // token认证错误码
    TOKEN_ERROR(100),
    //权限错误
    ACCESS_ERROR(300),
    //参数异常
    PARAM_ERROR(400),
    //服务端异常
    SERVER_ERROR(500);

    private final int code;

    CodeNumEnum(int code) {
        this.code = code;
    }

    @JsonValue
    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "CodeNumEnum{" +
                   "code=" + code +
                   '}';
    }
}
