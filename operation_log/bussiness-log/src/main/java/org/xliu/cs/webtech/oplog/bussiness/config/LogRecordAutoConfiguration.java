package org.xliu.cs.webtech.oplog.bussiness.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xliu.cs.webtech.oplog.bussiness.func.IParseFunction;
import org.xliu.cs.webtech.oplog.bussiness.func.ParseFunctionFactory;
import org.xliu.cs.webtech.oplog.bussiness.func.impl.DefaultParseFunction;
import org.xliu.cs.webtech.oplog.bussiness.log.DefaultLogRecordServiceImpl;
import org.xliu.cs.webtech.oplog.bussiness.log.ILogRecordService;

import java.util.List;

@Configuration
@Slf4j
public class LogRecordAutoConfiguration {


    @Bean
    public ParseFunctionFactory parseFunctionFactory(@Autowired List<IParseFunction> parseFunctions) {
        return new ParseFunctionFactory(parseFunctions);
    }

    @Bean
    @ConditionalOnMissingBean(IParseFunction.class)
    public IParseFunction parseFunction() {
        return new DefaultParseFunction();
    }
    
    /**
     * 默认记录日志的方式
     */
    @Bean
    @ConditionalOnMissingBean(ILogRecordService.class)
    public ILogRecordService recordService() {
        return new DefaultLogRecordServiceImpl();
    }
}