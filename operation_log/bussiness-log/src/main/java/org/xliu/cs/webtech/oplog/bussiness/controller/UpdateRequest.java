package org.xliu.cs.webtech.oplog.bussiness.controller;

import lombok.Data;

@Data
public class UpdateRequest {
    private Long orderId;
}
