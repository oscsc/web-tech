package org.xliu.cs.webtech.oplog.bussiness.io;


public final class CodeMsg {

    private final CodeNumEnum code;
    private final String message;


    public CodeMsg(CodeNumEnum code, String message) {
        this.code = code;
        this.message = message;
    }

    public CodeNumEnum getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


    @Override
    public String toString() {
        return "CodeMsg [code=" + code.getCode() + ", message=" + message + "]";
    }


}
