package org.xliu.cs.webtech.oplog.bussiness.func;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

public class ParseFunctionFactory {
    private Map<String, IParseFunction> allFunctionMap = Collections.emptyMap();

    public ParseFunctionFactory(List<IParseFunction> parseFunctions) {
        if (CollectionUtils.isEmpty(parseFunctions)) {
            return;
        }
        allFunctionMap = new HashMap<>();
        for (IParseFunction parseFunction : parseFunctions) {
            if (!StringUtils.hasLength(parseFunction.functionName())) {
                continue;
            }
            allFunctionMap.put(parseFunction.functionName(), parseFunction);
        }
    }

    private IParseFunction getFunction(String functionName) {
        return allFunctionMap.get(functionName);
    }

    public boolean isBeforeFunction(String functionName) {
        return Optional.ofNullable(getFunction(functionName))
                .map(IParseFunction::executeBefore)
                .orElse(false);
    }

    public String getFunctionReturnValue(Map<String, String> beforeFunctionNameAndReturnMap, String value, String expression, String functionName) {
        if (!StringUtils.hasLength(functionName)) {
            // TODO: null ? or a specific value?
            return value == null ? "" : value;
        }
        String functionReturnValue;
        String functionCallInstanceKey = getFunctionCallInstanceKey(functionName, expression);
        if (beforeFunctionNameAndReturnMap != null && beforeFunctionNameAndReturnMap.containsKey(functionCallInstanceKey)) {
            functionReturnValue = beforeFunctionNameAndReturnMap.get(functionCallInstanceKey);
        } else {
            functionReturnValue = apply(functionName, value);
        }
        return functionReturnValue;
    }

    private String apply(String functionName, String value) {
        IParseFunction function = getFunction(functionName);
        if (function == null) {
            // TODO: null ? or a specific value?
            return value;
        }
        return function.apply(value);
    }

    public String getFunctionCallInstanceKey(String functionName, String expression) {
        return functionName + "@" + expression;
    }
}
