package org.xliu.cs.webtech.oplog.bussiness.func.impl;

import org.xliu.cs.webtech.oplog.bussiness.func.IParseFunction;
import org.springframework.stereotype.Component;


@Component
public class GetOldUserFunction implements IParseFunction {
    @Override
    public boolean executeBefore() {
        return true;
    }

    @Override
    public String functionName() {
        return "getOldUser";
    }

    @Override
    public String apply(String value) {
        return "old_" + value;
    }
}
