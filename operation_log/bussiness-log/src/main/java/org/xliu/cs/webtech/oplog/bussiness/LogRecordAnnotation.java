package org.xliu.cs.webtech.oplog.bussiness;


import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface LogRecordAnnotation {
    String value();
    
    String bizNo();

}