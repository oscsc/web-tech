package org.xliu.cs.webtech.oplog.bussiness.func.impl;

import org.xliu.cs.webtech.oplog.bussiness.func.IParseFunction;
import org.springframework.stereotype.Component;

@Component
public class GetUserNameFunction implements IParseFunction {
    @Override
    public boolean executeBefore() {
        return true;
    }

    @Override
    public String functionName() {
        return "getUserName";
    }

    @Override
    public String apply(String value) {
        return "name_" + value;
    }
}
