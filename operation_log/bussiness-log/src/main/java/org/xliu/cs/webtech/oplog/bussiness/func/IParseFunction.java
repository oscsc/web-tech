package org.xliu.cs.webtech.oplog.bussiness.func;

public interface IParseFunction {

    /**
     * 自定义函数是否在业务代码执行之前解析，比如在更新操作时，需要查询旧值
     */
    default boolean executeBefore(){
        return false;
    }

    String functionName();

    String apply(String value);
}