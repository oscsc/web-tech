package org.xliu.cs.webtech.oplog.bussiness.io;

import lombok.Data;

@Data
public class Result<T> {
    private CodeNumEnum code;
    private String message;
    private T data;

    public static final CodeMsg SUCCESS = new CodeMsg(CodeNumEnum.SUCCESS, "操作成功");


    /**
     * 成功时候的调用，快捷调用
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(SUCCESS, data);
    }

    public static <T> Result<T> success() {
        return new Result<>(SUCCESS);
    }

    public static <T> Result<T> failed(String message) {
        return new Result<>(new CodeMsg(CodeNumEnum.SERVER_ERROR, message), null);
    }

    
    /**
     * for json deserialize
     */
    private Result() {
    }

    private Result(CodeMsg msg, T data) {
        this(msg);
        this.data = data;
    }

    private Result(CodeMsg codeMsg) {
        if (codeMsg != null) {
            this.code = codeMsg.getCode();
            this.message = codeMsg.getMessage();
        }
    }

    public boolean isSuccess() {
        return code == CodeNumEnum.SUCCESS;
    }

}
