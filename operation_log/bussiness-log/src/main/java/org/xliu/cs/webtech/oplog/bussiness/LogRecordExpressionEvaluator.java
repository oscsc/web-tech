package org.xliu.cs.webtech.oplog.bussiness;

import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.context.expression.CachedExpressionEvaluator;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LogRecordExpressionEvaluator extends CachedExpressionEvaluator {

    /**
     * 缓存方法、表达式和 SpEL 的 Expression 的对应关系
     */
    private final Map<ExpressionKey, Expression> expressionCache = new ConcurrentHashMap<>(64);


    public String parseExpression(String conditionExpression, AnnotatedElementKey key, EvaluationContext evalContext) {
        return getExpression(this.expressionCache, key, conditionExpression).getValue(evalContext, String.class);
    }
}
