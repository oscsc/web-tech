package org.xliu.cs.webtech.oplog.bussiness.log;

import org.xliu.cs.webtech.oplog.bussiness.LogRecordOp;

public interface ILogRecordService {
    /**
     * 保存 log
     *
     * @param logRecord 日志实体
     */
    void record(LogRecordOp logRecord);

}