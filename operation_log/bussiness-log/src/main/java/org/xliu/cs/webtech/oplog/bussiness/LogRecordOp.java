package org.xliu.cs.webtech.oplog.bussiness;

import lombok.Data;

@Data
public class LogRecordOp {
    /** 修改内容 */
    String content;

    /** 操作人 */
    Long operator;

    /** 唯一标识 */
    String bizNo;
    
    /** 修改成功还是失败 */
    String status;

    public LogRecordOp(String content, Long operator, String bizNo) {
        this.content = content;
        this.operator = operator;
        this.bizNo = bizNo;
    }
}
