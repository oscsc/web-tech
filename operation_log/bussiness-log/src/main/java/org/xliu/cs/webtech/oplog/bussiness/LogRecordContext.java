package org.xliu.cs.webtech.oplog.bussiness;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class LogRecordContext {

    private LogRecordContext() {
    }

    /**
     * 线程私有堆栈，防止方法调用链中出现多个切面函数导致变量被移除的情况。
     * 理论上，应该只在 Controller 的方法中实现，不应该在内部函数中注解。
     */
    private static final InheritableThreadLocal<Stack<Map<String, Object>>> variableMapStack = new InheritableThreadLocal<>();


    public static void putVariable(String name, Object value) {
        variableMapStack.get().peek().put(name, value);
    }

    public static Map<String, Object> getVariables() {
        return variableMapStack.get().peek();
    }

    public static Long getUserId() {
        return (Long) getVariables().get("userId");
    }

    public static void setUserId(Long value) {
        putVariable("userId", value);
    }


    public static void clear() {
        variableMapStack.remove();
    }

    public static void putEmptySpan() {
        if (variableMapStack.get() == null) {
            variableMapStack.set(new Stack<>());
        }
        variableMapStack.get().push(new HashMap<>());
    }
}
