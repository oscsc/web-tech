package org.xliu.cs.webtech.oplog.bussiness.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.xliu.cs.webtech.oplog.bussiness.LogRecordAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xliu.cs.webtech.oplog.bussiness.io.Result;

@Controller
@RequestMapping("/order")
public class OrderController {

    @LogRecordAnnotation(
            value = "修改了订单的配送员：从'{getOldUser{#request.getOrderId()}}', 修改到'{getUserName{#userId}}'",
            bizNo="#request.getOrderId()")
    @PostMapping("/update")
    @ResponseBody
    public Result<Void> update(@RequestBody UpdateRequest request) {
        return Result.success();
    }

}
