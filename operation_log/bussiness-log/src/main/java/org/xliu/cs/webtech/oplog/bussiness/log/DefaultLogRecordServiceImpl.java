package org.xliu.cs.webtech.oplog.bussiness.log;

import lombok.extern.slf4j.Slf4j;
import org.xliu.cs.webtech.oplog.bussiness.LogRecordOp;

@Slf4j
public class DefaultLogRecordServiceImpl implements ILogRecordService {

    @Override
//    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void record(LogRecordOp logRecord) {
        log.info("【logRecord】log={}", logRecord);
    }
}