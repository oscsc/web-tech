package org.xliu.cs.webtech.oplog.bussiness;

import org.xliu.cs.webtech.oplog.bussiness.controller.UpdateRequest;
import org.xliu.cs.webtech.oplog.bussiness.func.impl.GetOldUserFunction;
import org.junit.jupiter.api.Test;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;
import java.util.*;


class EvaluationContextTest {


    static String showInt(String name, String weight) {
        return "showInt" + name + weight;
    }

    @Test
    void testShowInt() throws NoSuchMethodException {

        Method method = EvaluationContextTest.class.getDeclaredMethod("showInt", String.class, String.class);
        Object[] args = new Object[2];
        args[0] = "123";
        args[1] = "444";
        MethodBasedEvaluationContext context = new MethodBasedEvaluationContext(null, method, args, new DefaultParameterNameDiscoverer());

        ExpressionParser parser = new SpelExpressionParser();
        Expression expr3 = parser.parseExpression("我的名字是:{#parseInt.apply(#name)} ,体重是:{#weight}", new TemplateParserContext("{", "}"));

        GetOldUserFunction function = new GetOldUserFunction();

        context.setVariable("parseInt", function);

        System.out.println(expr3.getValue(context, String.class));
        
    }
    
    @Test
    void test() throws NoSuchMethodException {
        ExpressionParser parser = new SpelExpressionParser();

        UpdateRequest request = new UpdateRequest();
        request.setOrderId(111L);
        request.setUserId(222L);
        
        Expression expr3 = parser.parseExpression("修改了订单的配送员：从“@@#getOldUser(#{#request.getOrderId()})@@”, 修改到“@@#getUserName(#{#request.getUserId()})@@”",
                new TemplateParserContext("${", "}"));

        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("request", request);
        
        
        System.out.println(expr3.getValue(context, String.class));
        
    }

    @Test
    public void testVariableExpression() {
        ExpressionParser parser = new SpelExpressionParser();

        //1.测试集合或数组
        List<Integer> list = new ArrayList<Integer>();
        list.add(4);
        list.add(5);
        EvaluationContext context1 = new StandardEvaluationContext();
        context1.setVariable("list", list);
        Collection<Integer> result1 = parser.parseExpression("#list.![#this+1]").getValue(context1, Collection.class);
        result1.forEach(System.out::println);

        System.out.println("------------");
        //2.测试字典
        Map<String, Integer> map = new HashMap<>();
        map.put("a", 1);
        map.put("b", 2);
        EvaluationContext context2 = new StandardEvaluationContext();
        context2.setVariable("map", map);
        List<Integer> result2 = parser.parseExpression("#map.![value+1]").getValue(context2, List.class);
        result2.forEach(System.out::println);
    }
}