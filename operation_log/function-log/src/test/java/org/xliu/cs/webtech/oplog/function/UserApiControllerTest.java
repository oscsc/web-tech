package org.xliu.cs.webtech.oplog.function;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.xliu.cs.webtech.oplog.function.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class UserApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper ow = new ObjectMapper();

    @Test
    void apiOpLog() throws Exception {
        // see op log msg for exception
        mockMvc.perform(MockMvcRequestBuilders.get("/user?username=%s", "eddy"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        // see op log msg for success
        User user = new User();
        user.setUsername("eddy");
        // password is not recorded.
        user.setPassword("8888888");
        user.setPhone("15782359234");
        String requestJson = ow.writeValueAsString(user);
        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}