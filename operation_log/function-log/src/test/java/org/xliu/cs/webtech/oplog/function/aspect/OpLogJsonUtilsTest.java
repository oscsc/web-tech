package org.xliu.cs.webtech.oplog.function.aspect;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.xliu.cs.webtech.oplog.function.pojo.User;

import static org.junit.jupiter.api.Assertions.assertFalse;

class OpLogJsonUtilsTest {

    @Test
    void encode() throws JsonProcessingException {

        User user = new User();
        user.setUsername("eddy");
        // password is not recorded.
        user.setPassword("8888888");
        user.setPhone("15782359234");

        String result = OpLogJsonUtils.encode(user);

        assertFalse(result.contains("8888888"));
    }
}