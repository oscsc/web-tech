package org.xliu.cs.webtech.oplog.function.annotation;


import io.swagger.v3.oas.annotations.Operation;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 操作日志注解，使用 Operation 获取常规字段，避免使用多个注解。
 * 仅用于 Controller 中的方法。
 * 继承 @Operation
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Operation
public @interface LogExtension {

    /**
     * 是否记录请求体
     */
    boolean saveRequest() default false;

    /**
     * 是否记录响应体
     */
    boolean saveResponse() default false;

    /**
     * 等价于 Operation 的 method
     */
    @AliasFor(annotation = Operation.class)
    String method() default "";

    /**
     * 等价于 Operation 的 summary
     */
    @AliasFor(annotation = Operation.class)
    String summary() default "";

    /**
     * 等价于 Operation 的 operationId
     */
    @AliasFor(annotation = Operation.class)
    String operationId() default "";

}
