package org.xliu.cs.webtech.oplog.function.annotation;


import java.lang.annotation.*;

/**
 * 操作日志的字段不打印
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OpLogIgnore {

}
