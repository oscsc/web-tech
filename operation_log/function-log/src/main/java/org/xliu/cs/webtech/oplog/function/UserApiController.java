package org.xliu.cs.webtech.oplog.function;


import org.xliu.cs.webtech.oplog.function.annotation.LogExtension;
import org.xliu.cs.webtech.oplog.function.pojo.User;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Validated
@RestController
@Tag(name = "user", description = "Operations about user")
public class UserApiController {

    private final UserApiService service;

    public UserApiController(UserApiService service) {
        this.service = service;
    }

    /**
     * POST /user : Create user
     * This can only be done by the logged in user.
     *
     * @param user Created user object (required)
     * @return successful operation (status code 200)
     */
    @LogExtension(method = "POST", operationId = "createUser", summary = "创建用户", saveRequest = true, saveResponse = true)
    @PostMapping("/user")
    ResponseEntity<Void> createUser(
            @Parameter(name = "User", description = "Created user object", required = true) @Valid @RequestBody User user
    ) {
        return service.createUser(user);
    }


    /**
     * 获取用户，路径变量当前操作日志不会打印，因此Get的参数最好通过 Query 传递
     */
    @LogExtension(method = "GET", operationId = "getUserByName", summary = "获取用户", saveRequest = true, saveResponse = true)
    @GetMapping("/user")
    ResponseEntity<User> getUserByName(@Parameter(name = "username", description = "The name that needs to be fetched.", required = true)
                                       @RequestParam("username") String username) {
        return service.getUserByName(username);
    }
}
