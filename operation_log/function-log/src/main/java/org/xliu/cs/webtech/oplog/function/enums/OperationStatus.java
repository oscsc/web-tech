package org.xliu.cs.webtech.oplog.function.enums;

/**
 * 操作状态
 */
public enum OperationStatus {
    /**
     * 成功 (0)
     */
    SUCCESS,

    /**
     * 失败 (1)
     */
    FAIL,
}
