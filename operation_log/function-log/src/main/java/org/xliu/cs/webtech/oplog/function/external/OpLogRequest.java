package org.xliu.cs.webtech.oplog.function.external;

import lombok.Data;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 发送的操作日志的请求体
 */
@Data
public class OpLogRequest {

    /** 0 for success, 1 for failed */
    private int status;

    /** reason if failed */
    private String errorMsg;

    /** user */
    private String operator;

    private Date operationTime;

    /** 服务名 */
    private String serviceName;

    /** 模块名（controller） */
    private String moduleName;

    /** 操作名 */
    private String operationName;

    /** GET/POST/PUT */
    private String method;

    /** 请求参数（无论Get/Post/Put} */
    private String params;

    /** 请求结果 */
    private String result;


}
