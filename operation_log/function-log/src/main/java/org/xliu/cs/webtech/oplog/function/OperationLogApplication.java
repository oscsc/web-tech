package org.xliu.cs.webtech.oplog.function;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OperationLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(OperationLogApplication.class, args);
    }


}