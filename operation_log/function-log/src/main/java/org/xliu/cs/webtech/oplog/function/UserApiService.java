package org.xliu.cs.webtech.oplog.function;

import java.util.HashMap;
import java.util.Map;

import org.xliu.cs.webtech.oplog.function.pojo.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserApiService {

    private final Map<String, User> users = new HashMap<>();

    /**
     * POST /user : Create user
     * This can only be done by the logged in user.
     *
     * @param user Created user object (required)
     * @return successful operation (status code 200)
     */
    ResponseEntity<Void> createUser(User user) {
        users.put(user.getUsername(), user);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    /**
     * GET /user/{username} : Get user by user name
     *
     * @param username The name that needs to be fetched. Use user1 for testing. (required)
     * @return successful operation (status code 200)
     * or Invalid username supplied (status code 400)
     * or User not found (status code 404)
     */
    ResponseEntity<User> getUserByName(String username) {
        User user = users.get(username);
        if (user == null) {
            throw new IllegalArgumentException("user not exist");
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}
