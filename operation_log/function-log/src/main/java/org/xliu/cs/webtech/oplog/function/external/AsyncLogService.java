package org.xliu.cs.webtech.oplog.function.external;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 异步调用日志服务
 */
@Slf4j
@Service
public class AsyncLogService {

    /**
     * 保存系统日志记录
     */
    @Async
    public void saveSysLog(OpLogRequest opLog) {
        log.info("msg : {}", opLog);
    }
}
