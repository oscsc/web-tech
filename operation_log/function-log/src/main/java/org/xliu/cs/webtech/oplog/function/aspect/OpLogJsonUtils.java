package org.xliu.cs.webtech.oplog.function.aspect;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import org.xliu.cs.webtech.oplog.function.annotation.OpLogIgnore;

/**
 * 自定义操作日志打印，支持字段忽略 {@link OpLogIgnore}
 */
class OpLogJsonUtils {

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        // 处理注解，对于序列化和反序列化都会生效
        // TODO: 字段在序列化时进行脱敏或者加密；
        // TODO: 需要处理的类不是我们可以修改，即支持按照字段名称进行脱敏或者加密或者忽略；
        mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean hasIgnoreMarker(AnnotatedMember m) {
                OpLogIgnore ann = _findAnnotation(m, OpLogIgnore.class);
                return ann != null;
            }
        });
    }

    /**
     * 用于操作日志的请求体和返回体进行序列化操作（通过自定义的注解，忽略不进行序列化的字段）
     */
    public static String encode(Object obj) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }
}
