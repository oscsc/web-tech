package org.xliu.cs.webtech.oplog.function.aspect;


import org.xliu.cs.webtech.oplog.function.annotation.LogExtension;
import org.xliu.cs.webtech.oplog.function.external.AsyncLogService;
import org.xliu.cs.webtech.oplog.function.enums.OperationStatus;
import org.xliu.cs.webtech.oplog.function.external.OpLogRequest;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 操作日志记录处理
 */
@Slf4j
@Aspect
@Component
public class OpLogAspect {

    @Value("${spring.application.name:unknown}")
    private String serviceName;

    @Autowired
    private AsyncLogService asyncLogService;

    /**
     * 对于 @LogExtension 注解的方法，处理完请求后执行
     *
     * @param joinPoint 切点
     */
    @AfterReturning(pointcut = "@annotation(extension)", returning = "jsonResult")
    public void doAfterReturning(JoinPoint joinPoint, LogExtension extension, Object jsonResult) {
        handleLog(joinPoint, extension, null, jsonResult);
    }

    /**
     * 对于 @LogExtension 注解的方法，拦截异常操作
     *
     * @param joinPoint 切点
     * @param e         异常
     */
    @AfterThrowing(value = "@annotation(extension)", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, LogExtension extension, Exception e) {
        handleLog(joinPoint, extension, e, null);
    }

    protected void handleLog(final JoinPoint joinPoint, LogExtension extension, final Exception e, Object jsonResult) {
        try {
            Class<?> targetClass = joinPoint.getTarget().getClass();

            OpLogRequest opLog = new OpLogRequest();
            // 设置返回状态
            opLog.setStatus(OperationStatus.SUCCESS.ordinal());
            if (e != null) {
                opLog.setStatus(OperationStatus.FAIL.ordinal());
                opLog.setErrorMsg(StringUtils.substring(e.getMessage(), 0, 2000));
            }

            // TODO: 根据实际情形获取用户名
            String username = "user";
            if (StringUtils.isEmpty(username)) {
                opLog.setOperator(username);
            }
            // 操作名
            opLog.setOperationName(extension.summary());

            opLog.setMethod(extension.method());

            // 设置模块名
            Tag apiAnnotation = targetClass.getAnnotation(Tag.class);
            if (apiAnnotation != null) {
                String moduleName = apiAnnotation.name();
                opLog.setModuleName(moduleName);
            }

            // 服务名
            opLog.setServiceName(serviceName);

            // 操作时间，jackson 默认不支持 java 8 的 LocalDate 和 LocalTime，需要额外引入依赖和序列化注解
            opLog.setOperationTime(new Date());

            // 处理设置注解上的参数
            getControllerMethodDescription(joinPoint, extension, opLog, jsonResult);

            // 保存数据库
            asyncLogService.saveSysLog(opLog);
        } catch (Exception exp) {
            // 记录本地异常日志
            log.error("操作日志记录异常:", exp);
        }
    }

    /**
     * 获取注解中对方法的描述信息 用于Controller层注解
     */
    private void getControllerMethodDescription(JoinPoint joinPoint, LogExtension extension, OpLogRequest operLog, Object jsonResult) throws Exception {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            HttpServletRequest request = requestAttributes.getRequest();

            // 是否需要保存request，且类型支持持久化
            if (extension.saveRequest() && contentTypeSupportSave(request)) {
                // 获取参数的信息
                String requestValue = getRequestValue(joinPoint);
                // 防止参数过长，截断
                operLog.setParams(StringUtils.substring(requestValue, 0, 2000));
            }
        }
        // 是否需要保存response，参数和值
        if (extension.saveResponse() && jsonResult != null) {
            // 自定义的序列化类，字段特殊处理
            String responseValue = OpLogJsonUtils.encode(jsonResult);
            // 防止参数过长，截断
            operLog.setResult(StringUtils.substring(responseValue, 0, 2000));
        }
    }

    /**
     * 获取请求参数值
     */
    private String getRequestValue(JoinPoint joinPoint) {
        StringBuilder params = new StringBuilder();
        Object[] paramsArray = joinPoint.getArgs();

        if (paramsArray == null || paramsArray.length <= 0) {
            return params.toString().trim();
        }
        for (Object o : paramsArray) {
            if (o == null || isFilterObject(o)) {
                continue;
            }
            try {
                // 自定义的序列化类，字段特殊处理
                String str = OpLogJsonUtils.encode(o);
                params.append(str).append(" ");
            } catch (Exception e) {
                log.error("encode object in oplog error", e);
            }
        }
        return params.toString().trim();
    }

    private boolean contentTypeSupportSave(HttpServletRequest request) {
        String contentType = request.getContentType();
        // 只有 application/json 类型的允许记录
        return "application/json".equalsIgnoreCase(contentType);
    }

    /**
     * 判断是否需要过滤的对象。
     *
     * @param o 对象信息。
     * @return 如果是需要过滤的对象，则返回true；否则返回false。
     */
    public boolean isFilterObject(final Object o) {
        return o instanceof MultipartFile || o instanceof HttpServletRequest || o instanceof HttpServletResponse
                       || o instanceof BindingResult;
    }
}
