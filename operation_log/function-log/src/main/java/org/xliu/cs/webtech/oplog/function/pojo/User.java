package org.xliu.cs.webtech.oplog.function.pojo;

import javax.validation.constraints.Size;

import org.xliu.cs.webtech.oplog.function.annotation.OpLogIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * A User who is purchasing from the pet store
 */
@Data
@Schema(name = "User", description = "A User who is purchasing from the pet store")
public class User {

    @Schema(name = "username")
    @Size(min = 0, max = 20)
    private String username;

    @Schema(name = "email")
    private String email;

    /** 该字段在操作日志中不打印，如果添加 @JsonProperty，则会出问题 */
    @OpLogIgnore
    @Schema(name = "password")
    private String password;

    @Schema(name = "phone")
    private String phone;

}

