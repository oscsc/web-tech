# springboot-api-op-log

通过注解实现操作日志切面，对 Swagger 界面无影响。

`@LogExtension`: 通过 Spring 的 `@AliasFor` 功能实现继承 OpenAPI的 `@Operation` 注解；
- 需要操作日志记录，使用 `@LogExtension`;
- 不需要操作日志记录，只使用 `@Operation`；

`@OpLogIgnore`：用于 request/response 中的字段进行注解，表示日志中不记录该字段。
