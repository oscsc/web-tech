package com.xliu.webtech.cache.springcache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCacheManager {

    public static void main(String[] args) {
        SpringApplication.run(SpringCacheManager.class, args);
    }
}
