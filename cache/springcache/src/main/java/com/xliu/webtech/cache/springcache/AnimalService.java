package com.xliu.webtech.cache.springcache;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


// Cache 注解使用示例
@Service
@CacheConfig(cacheNames = "my-redis-cache1", cacheManager = "redisCacheManager")
public class AnimalService {

    // cacheResolver 动态确定给定方法调用应使用的缓存集合。这允许开发者在更复杂的应用场景下实现自定义的缓存命名策略
    // 或基于上下文信息（如用户、租户等）选择不同的缓存存储。
    @Cacheable(key = "#id")
    public Animal getAnimal(Integer id) {
        System.out.println("操作数据库，返回Animal");
        return new Animal(110, "cat", "fish");
    }

    /**
     * 使用@CachePut注解的方法，一定要有返回值，该注解声明的方法缓存的是方法的返回结果。
     * it always causes the method to be invoked and its result to be stored in the associated cache
     **/
    @CacheEvict(key = "#animal.getId()")
    public Animal setAnimal(Animal animal) {
        System.out.println("存入数据库");
        return animal;
    }

    /**
     * 清理缓存，beforeInvocation 默认为false表明，仅当方法执行成功后（未抛出异常），再进行缓存数据删除。
     */
    @CacheEvict(key = "#id", beforeInvocation = false)
    public void deleteAnimal(Integer id) {
        System.out.println("删除数据库中animal");
    }

    // 更新缓存，保证跟数据库同步
    @CacheEvict(key = "#id")
    public Animal updateAnimal(Animal animal) {
        System.out.println("修改animal,并存入数据库");
        return animal;
    }

}
