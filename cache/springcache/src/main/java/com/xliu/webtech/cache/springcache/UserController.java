package com.xliu.webtech.cache.springcache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
@Slf4j
public class UserController {

    private final AnimalService animalService;


    @Autowired
    public UserController(AnimalService animalService) {
        this.animalService = animalService;
    }

    @GetMapping("/queryAnimal")
    @ResponseBody
    public Animal queryAnimal(@RequestParam(value = "ID") Integer ID) {
        Animal animal = animalService.getAnimal(ID);
        log.info("animal " + animal.toString());
        return animal;
    }
}