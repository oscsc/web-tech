package com.xliu.webtech.cache.springcache;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// Redis Cache 配置示例
@Configuration
@EnableCaching
public class RedisCacheConfig {
    public static String SHORT_TTL_CACHE_NAMES = "short_ttl";
    public static String LONG_TTL_CACHE_NAMES = "long_ttl";

    @Value("${spring.redis.database}")
    private int database;

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.password}")
    private String password;

    @Value("${spring.redis.port}")
    private int port;

    @Value("${spring.redis.timeout}")
    private long timeout;

    @Value("${spring.redis.lettuce.shutdown-timeout}")
    private long shutDownTimeout;

    @Value("${spring.redis.lettuce.pool.max-idle}")
    private int maxIdle;

    @Value("${spring.redis.lettuce.pool.min-idle}")
    private int minIdle;

    @Value("${spring.redis.lettuce.pool.max-active}")
    private int maxActive;

    @Value("${spring.redis.lettuce.pool.max-wait}")
    private long maxWait;

    private final Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = initRedisSerializer();

    private Jackson2JsonRedisSerializer<Object> initRedisSerializer() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,
                ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);

        return new Jackson2JsonRedisSerializer<>(Object.class);
    }

    // Redis lettuceConnectionFactory 配置
    @Bean
    public LettuceConnectionFactory lettuceConnectionFactory() {
        GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
        genericObjectPoolConfig.setMaxIdle(maxIdle);
        genericObjectPoolConfig.setMinIdle(minIdle);
        genericObjectPoolConfig.setMaxTotal(maxActive);
        genericObjectPoolConfig.setMaxWait(Duration.ofMillis(maxWait));
        // 每隔多少毫秒，空闲线程驱逐器关闭多余的空闲连接
        // 默认是 -1，当该属性值为负值时，lettuce连接池要维护的最小空闲连接数的目标minIdle就不会生效
        genericObjectPoolConfig.setTimeBetweenEvictionRuns(Duration.ofMillis(100));

        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setDatabase(database);
        redisStandaloneConfiguration.setHostName(host);
        redisStandaloneConfiguration.setPort(port);
        redisStandaloneConfiguration.setPassword(RedisPassword.of(password));

        LettuceClientConfiguration clientConfig = LettucePoolingClientConfiguration.builder()
                .commandTimeout(Duration.ofMillis(timeout))
                .shutdownTimeout(Duration.ofMillis(shutDownTimeout))
                .poolConfig(genericObjectPoolConfig)
                .build();

        // shareNativeConnection 这个属性默认是true,允许多个连接公用一个物理连接
        // validateConnection这个属性是每次获取连接时，校验连接是否可用。默认false,不去校验。
        // 默认情况下，lettuce开启一个共享的物理连接，是一个长连接，所以默认情况下是不会校验连接是否可用的
        return new LettuceConnectionFactory(redisStandaloneConfiguration, clientConfig);
    }

    /**
     * Redis Template 配置
     *
     * Cache 底层使用 RedisCacheWriter 等操作，而不是使用 RedisTemplate。
     * 因此如果同时使用 RedisTemplate，需要保证 key/value 的序列化方式一致。
     * <p>
     * 覆盖 {@link org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration} 中的 RedisTemplate
     *
     * @param lettuceConnectionFactory
     * @return
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(LettuceConnectionFactory lettuceConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(lettuceConnectionFactory);
        // 使用Jackson2JsonRedisSerializer 替换默认的 JdkSerializationRedisSerializer 来序列化和反序列化redis的value值
        RedisSerializer<String> stringRedisSerializer = new StringRedisSerializer();
        // key采用String的序列化方式
        template.setKeySerializer(stringRedisSerializer);
        // hash的key也采用String的序列化方式
        template.setHashKeySerializer(stringRedisSerializer);
        // value序列化方式采用jackson
        template.setValueSerializer(jackson2JsonRedisSerializer);
        // hash的value序列化方式采用jackson
        template.setHashValueSerializer(jackson2JsonRedisSerializer);

        template.afterPropertiesSet();
        return template;
    }

    /**
     * Redis Cache Manager 配置，配置多个不同的Cache
     *
     * Cache 底层使用 RedisCacheWriter 等操作，而不是使用 RedisTemplate。
     * 因此如果同时使用 RedisTemplate，需要保证 key/value 的序列化方式一致。
     */
    @Bean("redisCacheManager")
    @Primary
    public CacheManager cacheManager(LettuceConnectionFactory lettuceConnectionFactory) {
        RedisSerializer<String> redisSerializer = new StringRedisSerializer();

        // 配置1 , usePrefix为true.这样生成的key都会带cacheName前缀,防止和其他业务的key重复
        RedisCacheConfiguration shortConfig = RedisCacheConfiguration.defaultCacheConfig()
                //缓存失效时间
                .entryTtl(Duration.ofSeconds(2))
                //key序列化方式
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(redisSerializer))
                //value序列化方式
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(jackson2JsonRedisSerializer))
                //不允许缓存null值
                .disableCachingNullValues();

        //配置2, usePrefix为true.这样生成的key都会带cacheName前缀,防止和其他业务的key重复
        RedisCacheConfiguration longConfig = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofMinutes(30))
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(redisSerializer))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(jackson2JsonRedisSerializer))
                .disableCachingNullValues();


        //设置一个初始化的缓存空间set集合
        Set<String> cacheNames = new HashSet<>();
        cacheNames.add(SHORT_TTL_CACHE_NAMES);
        cacheNames.add(LONG_TTL_CACHE_NAMES);

        //对每个缓存空间应用不同的配置
        Map<String, RedisCacheConfiguration> configurationMap = new HashMap<>(3);
        configurationMap.put(SHORT_TTL_CACHE_NAMES, shortConfig);
        configurationMap.put(LONG_TTL_CACHE_NAMES, longConfig);

        return RedisCacheManager.builder(lettuceConnectionFactory)
                //默认缓存配置，如果 cacheNames 找不到对应的 cache（allowInFlightCacheCreation 允许动态创建），则使用该配置
                .cacheDefaults(shortConfig)
                //初始化缓存空间
                .initialCacheNames(cacheNames)
                //初始化缓存配置，创建相应的 Cache
                .withInitialCacheConfigurations(configurationMap).build();

    }


}