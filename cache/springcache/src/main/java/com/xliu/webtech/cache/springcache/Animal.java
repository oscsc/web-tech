package com.xliu.webtech.cache.springcache;

public class Animal {
    private final int id;
    private final String name;
    private final String food;

    public Animal(int id, String name, String food) {
        this.id = id;
        this.name = name;
        this.food = food;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFood() {
        return food;
    }
}
