package org.xliu.cs.microservice.springcloud.gateway;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.springdoc.core.AbstractSwaggerUiConfigProperties;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.SwaggerUiConfigProperties;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class SwaggerProvider {

    public static final String API_URI = "/%s/v3/api-docs";

    /**
     * 在gateway启动时获取服务，不会定时刷新，可以通过 @Schedule 注解定时执行。
     */
    @Bean
    public List<GroupedOpenApi> apis(SwaggerUiConfigProperties configProperties, RouteDefinitionLocator routeLocator) {
        List<GroupedOpenApi> groups = new ArrayList<>();

        // 获取所有可用的服务地址
        List<RouteDefinition> definitions = routeLocator.getRouteDefinitions().collectList().block();
        if (CollectionUtils.isEmpty(definitions)) {
            return groups;
        }
        Set<AbstractSwaggerUiConfigProperties.SwaggerUrl> urls = new HashSet<>();
        definitions.stream().filter(route -> route.getUri().getHost() != null)
            .distinct()
            .forEach(route -> {
                    // service low case
                    String name = route.getUri().getHost().toLowerCase(Locale.ROOT);
                    // 排查 Eureka 和网关服务
                    if (!name.contains("eureka") && !name.contains("gateway")) {
                        AbstractSwaggerUiConfigProperties.SwaggerUrl swaggerUrl = new AbstractSwaggerUiConfigProperties.SwaggerUrl();
                        swaggerUrl.setName(name);
                        swaggerUrl.setUrl(String.format(API_URI, name));
                        urls.add(swaggerUrl);
                    }
                }
            );

        configProperties.setUrls(urls);
        return groups;
    }

}