# Spring Gateway 示例和Swagger整合

采用Eureka作为服务发现/注册，因为可以直接运行，不需要外部服务依赖。

Gateway 提供统一的 swagger ui地址。

启动：
- 运行 EurekaServerApplication，访问 localhost:16060
- 运行 GateWayApplication，访问 http://localhost:16061/swagger-ui.html
- 运行 AppApplication，通过网关访问 http://localhost:16061/app/test/string