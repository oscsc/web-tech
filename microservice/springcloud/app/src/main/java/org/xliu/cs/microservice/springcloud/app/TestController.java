package org.xliu.cs.microservice.springcloud.app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/test")
@Controller
public class TestController {
    
    @GetMapping("/string")
    @ResponseBody
    public Result<String> test() {
        return Result.of("Hello, App");
    }


    @GetMapping("/int")
    @ResponseBody
    public Result<Integer> callInt() {
        return Result.of(1);
    }
}
