package org.xliu.cs.microservice.springcloud.app;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema
public class Result<T> {

    @Schema
    private T data;

    public static <F> Result<F> of(F t) {
        Result<F> result = new Result<>();
        result.setData(t);
        return result;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
