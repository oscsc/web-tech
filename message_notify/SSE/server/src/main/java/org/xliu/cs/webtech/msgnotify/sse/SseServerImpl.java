package org.xliu.cs.webtech.msgnotify.sse;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Service
public class SseServerImpl implements SseServer {
    private static final Logger LOG = LoggerFactory.getLogger(SseServerImpl.class);

    @Override
    public SseEmitter connect(String userId) {
        // TODO(lzq) 如果同一个Client浏览器界面被复制，其表现如何？
        if (SseSession.exists(userId)) {
            SseSession.remove(userId);
        }
        // 设置 time out 时间为无限制，生产环境会导致导致推送对象在 seesionMap 一直保存。
        SseEmitter sseEmitter = new SseEmitter(0L);
        sseEmitter.onError((err) -> {
            LOG.error("type: SseSession Error, msg: {} session Id : {}", err.getMessage(), userId);
            SseSession.onError(userId, err);
        });

        sseEmitter.onTimeout(() -> {
            LOG.info("type: SseSession Timeout, session Id : {}", userId);
            SseSession.remove(userId);
        });

        sseEmitter.onCompletion(() -> {
            LOG.info("type: SseSession Completion, session Id : {}", userId);
            SseSession.remove(userId);
        });
        SseSession.add(userId, sseEmitter);
        return sseEmitter;
    }

    @Override
    public boolean send(String userId, String content) {
        if (!SseSession.exists(userId)) {
            throw new IllegalArgumentException("User Id " + userId + " not Found");
        }
        try {
            SseSession.send(userId, content);
            return true;
        } catch (IOException exception) {
            LOG.error("type: SseSession send Error:IOException, msg: {} session Id : {}", exception.getMessage(), userId);
            // TODO 是否需要删除？
        }
        return false;
    }

    @Override
    public boolean close(String userId) {
        LOG.info("type: SseSession Close, session Id : {}", userId);
        return SseSession.remove(userId);
    }

}