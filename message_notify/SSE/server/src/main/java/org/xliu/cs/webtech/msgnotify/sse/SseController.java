package org.xliu.cs.webtech.msgnotify.sse;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
@RequestMapping
@CrossOrigin
public class SseController {
    private final SseServer sseServer;

    public SseController(SseServer sseServer) {
        this.sseServer = sseServer;
    }

    /** SSE接口，客户端拿到之后，注册 onmessage, onopen, onclose 处理 */
    @GetMapping(value = "/connect", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter connect(@RequestParam String clientId){
        return sseServer.connect(clientId);
    }


    /** 普通的接口 */
    @PostMapping(value = "/send")
    public String sendMessage( @RequestBody SendMegRequest sendMegRequest){
        if(sseServer.send(sendMegRequest.getClientId(), sendMegRequest.getSendMsg())){
            return "Success";
        }
        return "Faild";
    }
}