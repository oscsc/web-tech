package org.xliu.cs.webtech.msgnotify.sse;

import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class SseSession {

    // 每个 ClientId 可能会有多个连接，比如用户可以复制同一个页面
    private static final Map<String, SseEmitter> SESSIONS = new ConcurrentHashMap<>();

    public static void add(String sessionKey, SseEmitter sseEmitter) {
        if (SESSIONS.get(sessionKey) != null) {
            return;
        }
        SESSIONS.put(sessionKey, sseEmitter);
    }

    public static boolean exists(String sessionKey) {
        return SESSIONS.get(sessionKey) != null;
    }

    public static SseEmitter get(String sessionKey) {
        return SESSIONS.get(sessionKey);
    }

    public static boolean remove(String sessionKey) {
        SseEmitter sseEmitter = SESSIONS.get(sessionKey);
        if (sseEmitter != null) {
            sseEmitter.complete();
            SESSIONS.remove(sessionKey);
        }
        return false;
    }

    public static void onError(String sessionKey, Throwable throwable) {
        SseEmitter sseEmitter = SESSIONS.get(sessionKey);
        if (sseEmitter != null) {
            sseEmitter.completeWithError(throwable);
        }
    }

    private static AtomicInteger id = new AtomicInteger();

    public static void send(String sessionKey, String content) throws IOException {
        // 默认 event 类型（即name)为 message，如果填写其它类型，则前端需要通过 addEventListener 进行监听
        SseEmitter.SseEventBuilder builder = SseEmitter.event()
                .data(content)
                .id(String.valueOf(id.getAndIncrement()))
                .reconnectTime(3000)
                .name("message");

        SESSIONS.get(sessionKey).send(builder);
    }
}
