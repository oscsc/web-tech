package org.xliu.cs.webtech.msgnotify.sse;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public interface SseServer {
    SseEmitter connect(String userId);

    boolean send(String userId, String content);

    boolean close(String userId);

}
