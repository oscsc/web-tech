package org.xliu.cs.webtech.msgnotify.sse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendMegRequest {

    private String clientId;
    private String sendMsg;
}
