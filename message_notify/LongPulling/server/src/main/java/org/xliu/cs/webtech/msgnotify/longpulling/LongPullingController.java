package org.xliu.cs.webtech.msgnotify.longpulling;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.Collection;
import java.util.Date;

@RestController
@RequestMapping
@CrossOrigin
@Slf4j
public class LongPullingController {
    // 存放监听某个Id的长轮询集合 线程同步结构
    // DeferredResult 采用 Object 的 Equals 方法，因此每个实例都不一样
    public static Multimap<String, DeferredResult<String>> watchRequests = Multimaps.synchronizedMultimap(HashMultimap.create());

    private static final long TIME_OUT_MS = 15000;

    /**
     * 设置监听
     */
    @GetMapping(path = "watch/{id}")
    public DeferredResult<String> watch(@PathVariable String id) {
        log.info("receive watch {}", id);
        // 延迟对象设置超时时间
        DeferredResult<String> deferredResult = new DeferredResult<>(TIME_OUT_MS);
        // 异步请求完成时（包括超时后，也会调用）移除 key，防止内存溢出
        // 超时之后，默认返回是 503
        // {"timestamp":"2023-02-16T02:23:21.704+00:00","status":503,"error":"Service Unavailable","path":"/watch/111"}
        deferredResult.onCompletion(() -> {
            log.info("complete remove watch");
            watchRequests.remove(id, deferredResult);
        });
        // timeout 定制处理逻辑
        deferredResult.onTimeout(() -> {
            log.info("timeout, do nothing");
        });

        deferredResult.onError((t) -> {
            log.info("error:", t);
        });
        // 注册长轮询请求
        watchRequests.put(id, deferredResult);
        return deferredResult;
    }

    /**
     * 变更数据
     */
    @GetMapping(path = "publish/{id}")
    public String publish(@PathVariable String id) {
        // 数据变更 取出监听ID的所有长轮询请求，并一一响应处理
        Collection<DeferredResult<String>> deferredResults = watchRequests.get(id);
        if (!deferredResults.isEmpty()) {
            // 竞态分析：此时请求过期，success 返回 false，客户端还没有新的连接建立
            for (DeferredResult<String> deferredResult : deferredResults) {
                boolean success = deferredResult.setResult("我更新了" + new Date());
                log.info("return msg to watch {}: {}", id, success);
            }
        }

        // TODO: 如果消息没有通知到，则如何处理？
        return "success";
    }
}