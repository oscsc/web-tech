package org.xliu.cs.webtech.msgnotify.longpulling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LongPullingApplication {
    public static void main(String[] args) {
        SpringApplication.run(LongPullingApplication.class);
    }
}
