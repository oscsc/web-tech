package org.xliu.cs.webtech.msgnotify.longpulling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;

@ControllerAdvice
public class AsyncTimeoutHandler {

    // 对超时异常做处理，不再返回503，而是返回408
    @ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
    @ResponseBody
    @ExceptionHandler(AsyncRequestTimeoutException.class)
    public String asyncRequestTimeoutHandler(AsyncRequestTimeoutException e) {
        return "304";
    }
}