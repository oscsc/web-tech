# 长轮询



## 运行

IDEA 导入工程，直接运行。

浏览器地址：`localhost:8080/longrunning.html`

- 浏览器会发起请求 `localhost:8080/watch/111`

**实时通知**

- 通过接口调用`localhost:8080/publish/111`，可以发现浏览器数据得到更新；

**超时重连**

- 超时未发送消息，F12 可以看到浏览器会重新发起请求；
