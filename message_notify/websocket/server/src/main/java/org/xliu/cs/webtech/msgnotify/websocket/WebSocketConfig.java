package org.xliu.cs.webtech.msgnotify.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import java.util.List;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    private TaskScheduler messageBrokerTaskScheduler;

    // need @Lazy to avoid a cycle between the built-in WebSocket configuration and your WebSocketMessageBrokerConfigurer
    @Autowired
    public void setMessageBrokerTaskScheduler(@Lazy TaskScheduler taskScheduler) {
        this.messageBrokerTaskScheduler = taskScheduler;
    }


    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        // expose a STOMP endpoints over WebSocket without SockJS Fallback(downgrade to xhr-streaming, xhr-polling, and so on).
        // The Stomp client will attempt to connect to $servlet-path/websocket with websocket.（websocket 处理的是该路径）
        registry.addEndpoint("/websocket");
        // .withSockJS()

        // 权限相关一般是增加拦截器
        // HandshakeInterceptor 方式缺点是无法获取stomp connect header中的值，只能获取url中的参数，如果token用jwt等很长的，用这种方式实现并不友好。
        // addInterceptors()
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        // stomp：对于 websocket 的 send 消息的前缀，@MessageMapping都在该前缀范围下

        // STOMP messages whose destination header begins with /app are routed to @MessageMapping methods in @Controller classes.
        config.setApplicationDestinationPrefixes("/stomp");

        // enable a simple memory-based message broker for subscriptions and broadcasting and route messages whose destination
        // header begins with /topic `or `/queue to the broker(客户端只可以订阅这两个前缀的主题).
        config.enableSimpleBroker("/topic", "/user")
                // (server send heartbeat, client send heartbeat)
                .setHeartbeatValue(new long[]{25000, 0})
                .setTaskScheduler(messageBrokerTaskScheduler);

        // 针对单个用户的队列，每个用户不会冲突
        /*
         * 一对一发送的前缀
         * 订阅主题：/user/{userID}//demo3/greetings
         * 推送方式：1、@SendToUser("/demo3/greetings")
         *          2、messagingTemplate.convertAndSendToUser(destUsername, "/demo3/greetings", greeting);
         */
        // config.setUserDestinationPrefix("/user");

    }

    /**
     * 拦截器
     */
    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(new ChannelInterceptor() {
            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {
                StompHeaderAccessor accessor =
                        MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
                //1. 判断是否首次连接请求
                if (StompCommand.CONNECT.equals(accessor.getCommand())) {
                    //2. 验证是否登录
                    List<String> token = accessor.getNativeHeader("TOKEN");

                    if (token != null && !token.isEmpty() && "TOKEN".equals(token.get(0))) {
                        User user = new User("admin");
                        accessor.setUser(user);
                        return message;
                    }

                    throw new RuntimeException("Need Auth");
                }
                //不是首次连接，已经成功登陆
                return message;
            }
        });
    }

}