package org.xliu.cs.webtech.msgnotify.websocket;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HelloMessage {
    private String name;
}