package org.xliu.cs.webtech.msgnotify.websocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.concurrent.atomic.AtomicLong;

@Controller
@Slf4j
public class AliveController {

    @Autowired
    //消息发送模板
    SimpMessagingTemplate template;
    private final AtomicLong alive = new AtomicLong(0);

    @MessageMapping("/register") // 支持路径变量，用于处理 stompClient.send
    // 不能用 @EventListener(SessionConnectedEvent.class)， 因为建立连接之后才能进行订阅，不能在方法里进行发送
    // 用户注册，用于计算实时人数
    public void register(HelloMessage message) {
        long current = alive.incrementAndGet();

        // 发送给指定的 message broker（所有订阅者都会收到消息）
        // 也可以用 @SendTo("/topic/alive") 注解
        template.convertAndSend("/topic/alive", current);
    }


    //  The DISCONNECT may have been sent from the client or it may be automatically generated when the WebSocket session is closed.
    //  In some cases, this event is published more than once per session.
    //  Components should be idempotent with regard to multiple disconnect events.
    @EventListener(SessionDisconnectEvent.class)
    public void handle(SessionDisconnectEvent event) {
        // 处理 websocket 连接断开
        log.info("handle disconnect: {}", event);
        long current = alive.decrementAndGet();
        template.convertAndSend("/topic/alive", current);
    }

//    //定时任务发布消息（每十秒执行一次）
//    @Scheduled(cron = "*/10 * * * * ?")//运行周期时间可配
//    public void publicMarketValueMessage() {
//        int price = ThreadLocalRandom.current().nextInt(1000);
//        String destination = "/topic/price";
//        log.info("websocket 【{}】定时任务发布消息==>【开始】", destination);
//        try {
//            // 发送给指定的 message broker（所有订阅者都会收到消息）
//            this.template.convertAndSend(destination, price);
//        }catch (Exception e){
//            log.error("websocket ={} 定时任务发布消息==>【异常】", destination, e);
//        }
//    }
}