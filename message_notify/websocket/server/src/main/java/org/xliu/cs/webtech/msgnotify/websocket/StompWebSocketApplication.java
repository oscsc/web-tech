package org.xliu.cs.webtech.msgnotify.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StompWebSocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(StompWebSocketApplication.class, args);
    }
}