# Server Sent Event

**运行**

Server 端：
- IDEA 导入，直接运行，服务端地址 localhost:7077;

Client 端：
- 将 wsclient.html 放入 nginx 安装目录的 html 目录下面；

Nginx 代理：
- 将配置文件放入替换 nginx 安装目录conf 的 nginx.conf，启动 nginx，访问 localhost:8090/sseclient.html；

消息推送：
- 通过接口调用 POST localhost:8090/sse/send，请求体
```json
{
    "clientId": "11",
    "sendMsg": "hello2233"
}
```