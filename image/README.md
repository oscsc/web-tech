# 图像处理


## 人脸识别


[基于Opencv的人脸识别](src/main/java/org/xliu/cs/webtech/image/face/OpenCVFaceRecognize.java)

## 图片元数据处理


[CommonsImaging 元数据处理](src/main/java/org/xliu/cs/webtech/image/metadata/ImageMetaDataWriter.java)

- JPEG 中添加/更新 EXIF 元数据

- JPEG 中删除所有 EXIF 元数据

- JPEG 中删除单个 EXIF 元数据

- JPEG 中设置 EXIF 元数据中的 GPS 信息

## 图像转换


[灰度图的二值化算法（非0即1）](src/main/java/org/xliu/cs/webtech/image/transform/Binary.java)

- OpenCV 灰度二值化（阈值）算法：均值

- OpenCV 灰度二值化（阈值）算法：自适应阈值（Mean/Gauss），适合明暗变化很大的文本图片                                             

- OpenCV 灰度二值化（阈值）算法：指定阈值

- OpenCV 灰度二值化（阈值）算法：ostu（最大类间方差)

- OpenCV 灰度二值化（阈值）算法：wellner算法（周围N点以及上方一点)

[灰度处理](src/main/java/org/xliu/cs/webtech/image/transform/Gray.java)

- OpenCV 灰度化-平均值法

- OpenCV 灰度化-经验公式: gray=0.3*r+0.59*g+0.11*b

- OpenCV 灰度化-最大值法: 更亮些（白的区域多点)，丢失比较多的细节 

- OpenCV 原生图像灰度化

[图像压缩](src/main/java/org/xliu/cs/webtech/image/transform/ImageCompress.java)

- JDK ImageIO 图像压缩

- OpenCV ImageIO 图像压缩

