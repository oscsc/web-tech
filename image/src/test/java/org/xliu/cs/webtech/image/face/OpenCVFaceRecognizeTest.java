package org.xliu.cs.webtech.image.face;

import org.junit.jupiter.api.Test;

class OpenCVFaceRecognizeTest {
    @Test
    public void faceRecognize() {
        // 这里的路径跟 main 方法运行时，工作目录不一致
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        new OpenCVFaceRecognize(OpenCVFaceRecognize.DetectorType.ALT2).faceRecognize("data/a.jpg", "data/output/face_detector.jpg");
    }
}