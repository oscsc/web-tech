package org.xliu.cs.webtech.image.transform;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgproc.Imgproc;
import org.xliu.cs.projects.anno_for_doc.annotations.IgnoreNote;

import java.util.List;

@IgnoreNote
public class Utils {
    public static void showManyImages(List<Mat> srcImages) {
        showManyImages(srcImages, 200, CvType.CV_8UC1);
    }

    public static void showManyImages(List<Mat> srcImages, int nShowImageSize, int cvType) {
        int nNumImages = srcImages.size();
        Size nSizeWindows;
        if (nNumImages > 12) {
            System.out.println(" Not more than 12 images!");
            return;
        }
        // 根据图片序列数量来确定分割小窗口形态
        switch (nNumImages) {
            case 1:
                nSizeWindows = new Size(1, 1);
                break;
            case 2:
                nSizeWindows = new Size(2, 1);
                break;
            case 3:
            case 4:
                nSizeWindows = new Size(2, 2);
                break;
            case 5:
            case 6:
                nSizeWindows = new Size(3, 2);
                break;
            case 7:
            case 8:
                nSizeWindows = new Size(4, 2);
                break;
            case 9:
                nSizeWindows = new Size(3, 3);
                break;
            default:
                nSizeWindows = new Size(4, 3);
                break;
        }
        // 设置小图像尺寸，间隙，边界
        int nSplitLineSize = 15;
        int nAroundLineSize = 50;
        // 创建输出图像，图像大小根据输入源确定
        int imagesHeight = (int) (nShowImageSize * nSizeWindows.height + nAroundLineSize + (nSizeWindows.height - 1) * nSplitLineSize);
        int imagesWidth = (int) (nShowImageSize * nSizeWindows.width + nAroundLineSize + (nSizeWindows.width - 1) * nSplitLineSize);

        Mat showWindowImages = Mat.zeros(imagesHeight, imagesWidth, cvType);
        // 提取对应小图像的左上角坐标X，Y
        int posX = nAroundLineSize / 2;
        int posY = nAroundLineSize / 2;
        int tempPosX = posX;
        int tempPosY = posY;
        // 将每一小幅图像整合大图像
        for (int i = 0; i < nNumImages; i++) {
            // 小图像坐标转换
            if ((i % nSizeWindows.width == 0) && (tempPosX != posX)) {
                tempPosX = posX;
                tempPosY += (nSplitLineSize + nShowImageSize);
            }
            // 利用Rect区域将小图像置于大图像相应区域
            Mat tempImage = showWindowImages.submat(new Rect(tempPosX, tempPosY, nShowImageSize, nShowImageSize));
            // 利用resize函数实现图像缩放
            Imgproc.resize(srcImages.get(i), tempImage, new Size(nShowImageSize, nShowImageSize));
            tempPosX += (nSplitLineSize + nShowImageSize);
        }
        HighGui.imshow("show Images", showWindowImages);
    }
}
