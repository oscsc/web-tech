package org.xliu.cs.webtech.image.face;

import nu.pattern.OpenCV;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.xliu.cs.projects.anno_for_doc.annotations.ClassNote;

import java.io.File;

import static org.opencv.core.CvType.CV_8U;
import static org.opencv.core.CvType.CV_8UC3;
import static org.opencv.imgproc.Imgproc.COLOR_BGRA2GRAY;

/**
 * 人脸识别，使用 opencv java api.
 */
@ClassNote("基于Opencv的人脸识别")
public class OpenCVFaceRecognize {

    public enum DetectorType {
        DEFAULT("haarcascades/haarcascade_frontalface_alt.xml"),
        ALT("haarcascades/haarcascade_frontalface_alt.xml"),
        ALT2("haarcascades/haarcascade_frontalface_alt.xml");

        String path;

        DetectorType(String path) {
            this.path = path;
        }
    }

    static {
        // In Java 12+ loadShared() is not available. Use loadLocally() instead, and see notes below.
        // 但是如果 LoadLocally 被加载多次，会重复加载（因为是解压后用绝对路径加载）
        OpenCV.loadShared();
    }

    // 1 读取OpenCV自带的人脸识别特征XML文件 （正脸识别），因此对于侧脸的效果不是很好
    // 不支持全路径，只支持相对路径，或者 linux 下的全路径？
    private final CascadeClassifier faceDetector;

    public OpenCVFaceRecognize() {
        this(DetectorType.DEFAULT);
    }

    public OpenCVFaceRecognize(DetectorType type) {
        // 形式如 “/D:/repo/git_personal/web-tech/image/target/classes/haarcascades/haarcascade_frontalface_alt.xml”
        // 多个开头的 "/"，无法识别，因此需要用 new File 再获取 Path
        String modelFile = new File(OpenCVFaceRecognize.class.getClassLoader().getResource(type.path).getPath())
                .getPath();

        faceDetector = new CascadeClassifier(modelFile);
    }

    private Rect[] getFaces(Mat image) {
        // 2  特征匹配类
        MatOfRect face = new MatOfRect();
        // scaleFactor 默认值 1.1（每次图像尺寸减小的比例），minNeighbors 默认为3（每一个目标至少要被检测到3次才算是真的目标(因为周围的像素和不同的窗口大小都可以检测到人脸)，
        faceDetector.detectMultiScale(image, face);
        Rect[] rects = face.toArray();
        // remove native memory
        face.release();
        return rects;
    }

    public void faceRecognize(String inputPath, String outputPath) {
        // windows opencv can not read the classloader path
        Mat origin = Imgcodecs.imread(inputPath);
        System.out.printf("origin: channels [%d], size: [%s]\n", origin.channels(), origin.size());

        // 图片缩放进行识别，否则图片原像素太高，时间太慢
        // 即使这里用 CV_8U 也可以，后续 resize 时，会自动重新分批额 CV_8UC3 （三通道）的数据
        Mat image = new Mat(480, 640, CV_8UC3);
        Imgproc.resize(origin, image, image.size());
        origin.release();
        System.out.printf("image: channels [%d], size: [%s]\n", image.channels(), image.size());

        // 转灰度图
        Mat grey = new Mat(image.size(), CV_8U);
        Imgproc.cvtColor(image, grey, COLOR_BGRA2GRAY);
        System.out.printf("grey: channels [%d], size: [%s]\n", grey.channels(), grey.size());

        // 人脸识别并打印时间
        long startTime = System.currentTimeMillis();
        Rect[] faces = getFaces(grey);
        grey.release();
        long endTime = System.currentTimeMillis();
        System.out.println("匹配到 " + faces.length + " 个人脸");
        System.out.printf("time cost %f s\n", (endTime - startTime) / 1000.0);

        // 为每张识别到的人脸画一个圈
        // TODO: 是否应该在原图上进行绘制
        for (Rect rect : faces) {
            Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0), 2);
            Imgproc.putText(image, "", new Point(rect.x, rect.y), Imgproc.FONT_HERSHEY_SCRIPT_SIMPLEX, 1.0, new Scalar(0, 255, 0), 1, Imgproc.LINE_AA, false);
        }

        Imgcodecs.imwrite(outputPath, image);
//        HighGui.imshow("face-detector", resized);
//        HighGui.waitKey();

        image.release();
    }

    public static void main(String[] args) {
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        new OpenCVFaceRecognize().faceRecognize("image/data/a.jpg", "image/data/output/face_detector.jpg");

        // 这里需要显式退出，否则 main 程序不结束
        System.exit(0);
    }
}
