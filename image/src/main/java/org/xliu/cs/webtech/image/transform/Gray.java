package org.xliu.cs.webtech.image.transform;


import nu.pattern.OpenCV;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.xliu.cs.projects.anno_for_doc.annotations.ClassNote;
import org.xliu.cs.projects.anno_for_doc.annotations.MethodNote;

import java.util.Arrays;

@ClassNote("灰度处理")
public class Gray {


    @MethodNote("OpenCV 原生图像灰度化")
    public static Mat opencvGray(Mat src) {
        // 灰度化
        Mat dstImage = Mat.zeros(src.rows(), src.cols(), CvType.CV_8UC1);
        Imgproc.cvtColor(src, dstImage, Imgproc.COLOR_BGR2GRAY, 0);
        return dstImage;
    }

    @MethodNote("OpenCV 灰度化-平均值法")
    public static Mat averageGray(Mat src) {
        Mat mat = Mat.zeros(src.rows(), src.cols(), CvType.CV_8UC1);
        // 遍历图像的每个像素并修改
        for (int y = 0; y < src.rows(); y++) {
            for (int x = 0; x < src.cols(); x++) {
                // 假设我们修改的是BGR格式的图像，获取当前像素值
                double[] bgrValues = src.get(y, x);

                int average = (int) ((bgrValues[0] + bgrValues[1] + bgrValues[2]) / 3);

                mat.put(y, x, average);
            }
        }

        return mat;
    }

    @MethodNote("OpenCV 灰度化-最大值法: 更亮些（白的区域多点)，丢失比较多的细节 ")
    public static Mat maxGray(Mat src) {
        Mat mat = Mat.zeros(src.rows(), src.cols(), CvType.CV_8UC1);
        // 遍历图像的每个像素并修改
        for (int y = 0; y < src.rows(); y++) {
            for (int x = 0; x < src.cols(); x++) {
                // 假设我们修改的是BGR格式的图像，获取当前像素值
                double[] bgrValues = src.get(y, x);

                int average = (int) Math.max(Math.max(bgrValues[0], bgrValues[1]), bgrValues[2]);

                mat.put(y, x, average);
            }
        }

        return mat;
    }

    @MethodNote("OpenCV 灰度化-经验公式: gray=0.3*r+0.59*g+0.11*b")
    public static Mat experienceGray(Mat src) {
        Mat mat = Mat.zeros(src.rows(), src.cols(), CvType.CV_8UC1);
        // 遍历图像的每个像素并修改
        for (int y = 0; y < src.rows(); y++) {
            for (int x = 0; x < src.cols(); x++) {
                // 假设我们修改的是BGR格式的图像，获取当前像素值
                double[] bgrValues = src.get(y, x);

                int average = (int) (bgrValues[0] * 0.11 + bgrValues[1] * 0.59 + bgrValues[2] * 0.3 + 0.5);

                mat.put(y, x, average);
            }
        }

        return mat;
    }

    public static void main(String[] args) {

        String imagePath = "image/data/a.jpg";
        // 加载 opencv
        OpenCV.loadShared();

        Mat src = Imgcodecs.imread(imagePath);

        Mat gray = opencvGray(src);
        Mat averageGray = averageGray(src);
        Mat maxGray = maxGray(src);
        Mat experienceGray = experienceGray(src);

        Utils.showManyImages(Arrays.asList(gray, averageGray, maxGray, experienceGray), 600, CvType.CV_8UC1);
        HighGui.waitKey();

        src.release();
        gray.release();
        maxGray.release();
        experienceGray.release();
        averageGray.release();

        System.exit(-1);
    }
}
