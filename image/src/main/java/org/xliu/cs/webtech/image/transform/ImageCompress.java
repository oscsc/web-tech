package org.xliu.cs.webtech.image.transform;

import nu.pattern.OpenCV;
import org.apache.commons.imaging.*;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.xliu.cs.projects.anno_for_doc.annotations.ClassNote;
import org.xliu.cs.projects.anno_for_doc.annotations.MethodNote;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Iterator;

/**
 * 图像压缩
 * 1. JDK 实现； 2. OpenCV 实现
 * Commons-Imaging只负责图片的读写和元数据处理，读成 JDK 原生的 BufferedImage.
 */
@ClassNote("图像压缩")
public class ImageCompress {

    @MethodNote("JDK ImageIO 图像压缩")
    public static void jdkCompress(String imageFilePath, String imageOutPath) throws IOException {
        // 读取图片
        File input = new File(imageFilePath);
        BufferedImage image = ImageIO.read(input);

        // 根据图片格式，创建ImageWrite类
        Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");
        ImageWriter writer = writers.next();

        // 创建对应的文件流
        File compressedImageFile = new File(imageOutPath);
        OutputStream os = Files.newOutputStream(compressedImageFile.toPath());
        ImageOutputStream ios = ImageIO.createImageOutputStream(os);

        // 设置输出流
        writer.setOutput(ios);

        // 设置参数（压缩模式和压缩质量）
        ImageWriteParam param = writer.getDefaultWriteParam();
        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        // 值越大压缩率越小，但图片效果越好
        param.setCompressionQuality(0.1f);

        // 将图像写出
        writer.write(null, new IIOImage(image, null, null), param);

        os.close();
        ios.close();
        writer.dispose();
    }

    @MethodNote("OpenCV ImageIO 图像压缩")
    public static void opencvCompress(String imagePath, String imageOutPath) {
        // 加载 opencv
        OpenCV.loadShared();

        Mat src = Imgcodecs.imread(imagePath);

        // 第一个参数 IMWRITE_JPEG_QUALITY 表示对图片的质量进行改变，第二个是质量因子，1-100，值越大表示质量越高。
        MatOfInt params = new MatOfInt(Imgcodecs.IMWRITE_JPEG_QUALITY, 10);

        // 将 src 图片以质量进行暑促和
        Imgcodecs.imwrite(imageOutPath, src, params);

        // 释放数据
        src.release();
        params.release();
    }

    public static void main(String[] args) throws IOException {
        jdkCompress("image/data/a.jpg", "image/data/output/compress.jpg");

        opencvCompress("image/data/a.jpg", "image/data/output/opencv_compress.jpg");
    }

}
