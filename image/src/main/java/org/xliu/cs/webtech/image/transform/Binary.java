package org.xliu.cs.webtech.image.transform;

import nu.pattern.OpenCV;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.xliu.cs.projects.anno_for_doc.annotations.ClassNote;
import org.xliu.cs.projects.anno_for_doc.annotations.MethodNote;

import java.util.Arrays;

@ClassNote("灰度图的二值化算法（非0即1）")
public class Binary {


    @MethodNote("OpenCV 灰度二值化（阈值）算法：均值")
    public static Mat meanThreshold(Mat src) {
        assert src.type() == CvType.CV_8UC1;

        Mat mat = Mat.zeros(src.rows(), src.cols(), CvType.CV_8UC1);

        double mean = Core.mean(src).val[0];

        // 遍历图像的每个像素并修改
        for (int y = 0; y < src.rows(); y++) {
            for (int x = 0; x < src.cols(); x++) {
                // 假设我们修改的是BGR格式的图像，获取当前像素值
                double gray = src.get(y, x)[0];

                mat.put(y, x, gray > mean ? 255 : 0);
            }
        }
        return mat;
    }

    @MethodNote("OpenCV 灰度二值化（阈值）算法：指定阈值")
    public static Mat opencvThreshold(Mat src, double threshold) {
        assert src.type() == CvType.CV_8UC1;
        Mat dst = Mat.zeros(src.rows(), src.cols(), CvType.CV_8UC1);
        // 遍历图像的每个像素并修改
        Imgproc.threshold(src, dst, threshold, 255, Imgproc.THRESH_BINARY);

        return dst;
    }

    @MethodNote("OpenCV 灰度二值化（阈值）算法：自适应阈值（Mean/Gauss），适合明暗变化很大的文本图片                                             ")
    public static Mat opencvAdaptedThreshold(Mat src, int adaptedMethod, int blockSize, int C) {
        assert src.type() == CvType.CV_8UC1;
        Mat dst = Mat.zeros(src.rows(), src.cols(), CvType.CV_8UC1);
        // ADAPTIVE_THRESH_MEAN_C：小区域内取均值
        // ADAPTIVE_THRESH_GAUSSIAN_C：小区域内加权求和，权重是个高斯核
        // 15：小区域的面积，如 15 就是 15*15的小块
        // 5：最终阈值等于小区域计算出的阈值再减去此值
        Imgproc.adaptiveThreshold(src, dst, 255, adaptedMethod, Imgproc.THRESH_BINARY, blockSize, C);

        return dst;
    }

    @MethodNote("OpenCV 灰度二值化（阈值）算法：ostu（最大类间方差)")
    public static Mat ostuThreshold(Mat src) {
        assert src.type() == CvType.CV_8UC1;

        Mat mat = Mat.zeros(src.rows(), src.cols(), CvType.CV_8UC1);

        // 遍历图像的每个像素并修改
        Imgproc.threshold(src, mat, 0, 255, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
        return mat;
    }

    @MethodNote("OpenCV 灰度二值化（阈值）算法：wellner算法（周围N点以及上方一点)")
    public static Mat wellner(Mat src, double t) {
        assert src.type() == CvType.CV_8UC1;
        int width = src.width();
        int height = src.height();
        double factor = (100.0 - t) / 100.0;
        // s 表示参与计算的点的范围，一般取图像的1/8， t 一般取值为 15，factor为17/3
        int s = width / 8;

        // 假设灰度的平均值是 127，可以根据原矩阵先求出来
        double mean = 127;
        // 每一个点的上方的 gn 的值
        double[] prev_gn = new double[width];
        // 上方的点的均值
        Arrays.fill(prev_gn, mean * s);
        // 初始点（s个点的和）
        double gn = mean * s;

        Mat dst = Mat.zeros(height, width, CvType.CV_8UC1);

        for (int y = 0; y < height; y++) {
            // ------>
            for (int x = 0; x < width; x++) {
                double pn = src.get(y, x)[0];
                // g(n) = p(n) + (1.0 - 1.0 / s) * g(n-1)，前面的点g(n-1)的权重下降一个点，再加上p(n)，还是 s 个点的和
                // 虽然一直递增，但 p(0) 对 g(n) 的影响几乎没有
                gn = pn + (1.0 - 1.0 / s) * gn;
                double tn = (pn >= factor * (gn + prev_gn[x]) / 2.0 / s) ? 255 : 0;
                dst.put(y, x, tn);
                prev_gn[x] = gn;
            }
            y++;
            if (y == height) {
                break;
            }
            // <------
            for (int x = width - 1; x >= 0; x--) {
                double pn = src.get(y, x)[0];
                gn = pn + (1.0 - 1.0 / s) * gn;
                double tn = (pn >= factor * (gn + prev_gn[x]) / 2.0 / s) ? 255 : 0;
                dst.put(y, x, tn);
                prev_gn[x] = gn;
            }
        }
        return dst;
    }

    public static void main(String[] args) {

        String imagePath = "image/data/b.jpg";
        // 加载 opencv
        OpenCV.loadShared();

        Mat src = Imgcodecs.imread(imagePath, Imgcodecs.IMREAD_GRAYSCALE);

        Mat mean = meanThreshold(src);
        Mat opencvThreshold = opencvThreshold(src, 127);
        Mat ostuThreshold = ostuThreshold(src);
        Mat adaptedMeanThreshold = opencvAdaptedThreshold(src, Imgproc.ADAPTIVE_THRESH_MEAN_C, 15, 5);
        Mat adaptedGaussThreshold = opencvAdaptedThreshold(src, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, 15, 5);
        Mat wellner = wellner(src, 15);

        Utils.showManyImages(Arrays.asList(mean, opencvThreshold, ostuThreshold, adaptedMeanThreshold, adaptedGaussThreshold, wellner), 600, CvType.CV_8UC1);
        HighGui.waitKey();

        src.release();
        mean.release();
        opencvThreshold.release();
        adaptedMeanThreshold.release();
        adaptedGaussThreshold.release();
        ostuThreshold.release();
        wellner.release();

        System.exit(-1);
    }
}
