package org.xliu.cs.webtech.springboot.openapiyamlgenerator.pojo;

import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * A User who is purchasing from the pet store
 */
@Data
@Schema(name = "User", description = "A User who is purchasing from the pet store")
public class User {

    @Schema(name = "username")
    @JsonProperty
    @Size(min = 0, max = 20)
    private String username;

    @Schema(name = "email")
    @JsonProperty
    private String email;

    @Schema(name = "password")
    @JsonProperty
    private String password;

    @Schema(name = "phone")
    @JsonProperty
    private String phone;

}

