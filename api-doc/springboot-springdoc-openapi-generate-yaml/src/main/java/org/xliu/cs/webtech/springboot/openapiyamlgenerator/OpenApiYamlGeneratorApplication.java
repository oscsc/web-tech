package org.xliu.cs.webtech.springboot.openapiyamlgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenApiYamlGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenApiYamlGeneratorApplication.class, args);
    }


}