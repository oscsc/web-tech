package org.xliu.cs.webtech.springboot.openapiyamlgenerator;


import javax.validation.Valid;
import org.xliu.cs.webtech.springboot.openapiyamlgenerator.pojo.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@Validated
@RestController
@Tag(name = "user", description = "Operations about user")
public class UserApiController {

    private final UserApiService service;

    public UserApiController(UserApiService service) {
        this.service = service;
    }

    /**
     * POST /user : Create user
     * This can only be done by the logged in user.
     *
     * @param user Created user object (required)
     * @return successful operation (status code 200)
     */
    @Operation(
        operationId = "createUser",
        summary = "Create user",
        tags = { "user" },
        responses = {
            @ApiResponse(responseCode = "200", description = "successful operation")
        }
    )
    @PostMapping("/user")
    ResponseEntity<Void> createUser(
        @Parameter(name = "User", description = "Created user object", required = true) @Valid @RequestBody User user
    ) {
        return service.createUser(user);
    }



    /**
     * GET /user/{username} : Get user by user name
     *
     *
     * @param username The name that needs to be fetched. Use user1 for testing. (required)
     * @return successful operation (status code 200)
     *         or Invalid username supplied (status code 400)
     *         or User not found (status code 404)
     */
    @Operation(
        operationId = "getUserByName",
        summary = "Get user by user name",
        tags = { "user" },
        responses = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = {
                @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid username supplied"),
            @ApiResponse(responseCode = "404", description = "User not found")
        }
    )
    @GetMapping("/user/{username}")
    ResponseEntity<User> getUserByName(
        @Parameter(name = "username", description = "The name that needs to be fetched.", required = true) @PathVariable("username") String username
    ) {
        return service.getUserByName(username);
    }
}
